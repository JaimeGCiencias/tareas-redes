# Práctica 3
## Alvarez Cerrillo Mijail Aron
### 310020590

Ejecutamos el Makefile con make.

No se hizo actividad extra.

Ejercicio 1
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p3/imagenes/ejercicio1.png)

Ejercicio 2
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p3/imagenes/ejercicio2.png)

Ejercicio 3
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p3/imagenes/ejercicio3.png)

Ejercicio 4

UNO
```console
mijail:~$ sudo tcpdump "ether src 60:a4:4c:89:3c:a4" or "ether dst fc:fb:fb:01:fa:21"
```
DOS
```console
mijail:~$ sudo tcpdump "ether[0:4]=0x885395"
```
TRES
```console
mijail:~$ sudo tcpdump ip6
```
CUATRO
```console
mijail:~$ sudo tcpdump arp and "ether dst ff:ff:ff:ff:ff:ff"
```
CINCO
```console
mijail:~$ sudo tcpdump host 172
```
