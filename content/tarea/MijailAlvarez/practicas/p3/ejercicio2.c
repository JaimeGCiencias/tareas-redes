/* Práctica 3 : Manejo de archivos binarios y captura de paquetes
   Alvarez Cerrillo Mijail Aron 310020590
   Este programa representa el segundo ejercicio de la practica 3:
   Hacer una implementación sencilla del programa strings, es decir, identificar
   las cadenas conformadas por caracteres imprimibles consecutivos dentro de un
   archivo binario, cada cadena puede tener una longitud de uno a N caracteres. 
   Los caracteres imprimibles a ser tomados en cuenta son los que tienen un 
   rango ASCII de 32 a 126 (en decimal).*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Funcion que imprime el error y termina con el programa */
void error(char *msg) {
  perror(msg);
  exit(1);
}

/* Recorreremos el archivo binario caracter por caracter(byte por byte) que se
   nos presente como argumento de nuestro programa, obtendremos la Representacion
   decimal de ese caracter con la funcion fgetc, para vergificar que este en el
   rango adecuado (ASCII(rep decimal) 32 <= char <= 126)*/
int main(int argc, char *argv[]){
  //Read a File in BINARY mode ("rb")
  FILE *f = fopen(argv[1], "rb");
  if(f == NULL)
    error("No se pudo abrir el archivo solicitado.");
  int caracter;//Representacion Decimal de un caracter
  //Leemos caracter por caracter de nuestro archivo binario hasta EOF.
  while((caracter = fgetc(f)) != EOF) {
    if(caracter >= 32 && caracter <= 126)
      printf("%c", caracter);
     //El numero 10 es la representacion decimal(ASCII) de "\n"
    else if(caracter == 10)
      printf("%c", caracter);
  }
}
