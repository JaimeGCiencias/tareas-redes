/* Práctica 3 : Manejo de archivos binarios y captura de paquetes
   Alvarez Cerrillo Mijail Aron 310020590
   Este programa representa el primer ejercicio de la practica 3:
   Implementar un programa que funcione como el comando file, es decir, que
   reciba como argumentos una lista con nombres de archivos e identifique cuál
   es el formato de cada uno por medio de los números mágicos. Los formatos a
   ser identificados son GIF, ELF, PNG, ZIP, PDF, MP3 y EXE.*/
#include <stdio.h>
#include <stdlib.h>

/* Funcion que imprime el error y termina con el programa */
void error(char *msg) {
  perror(msg);
  exit(1);
}

/* Vamos a recorrer archivo por archivo y leer los primeros 8 bytes con fread,
   son los primeros 8 ya que el numero magigo mas grande que tenemos que revisar
   es el del formato png(tiene 8 bytes). Despues vamos a comparar los bytes
   leidos con los bytes de los numeros magicos que nos pide el ejercicio.*/
int main(int argc, char *argv[]) {
  if(argc < 2)
    error("Ingresa nombres de archivos como argumentos del programa.");
  for(int i = 1; i < argc; i ++) {
    //You should use "rb" if you're opening non-text files
    FILE *f = fopen(argv[i], "rb");
    unsigned char bytesMagicos[8];
    fread(bytesMagicos, 1, 8, f);
    //https://en.wikipedia.org/wiki/List_of_file_signatures
    switch (bytesMagicos[0]) {
    case 0x47://GIF 47 49 46 38 37 61 y 47 49 46 38 39 61
      if(bytesMagicos[1] == 0x49 && bytesMagicos[2] == 0x46 && bytesMagicos[3]
	 == 0x38 && bytesMagicos[4] == (0x37 || 0x39) && bytesMagicos[5] == 0x61)
	printf("%s: Archivo GIF\n", argv[i]);
      else
	printf("%s: Archivo no identificado\n", argv[i]);
      break;
    case 0x7F://ELF 7F 45 4C 46
      if(bytesMagicos[1] == 0x45 && bytesMagicos[2] == 0x4C && bytesMagicos[3]
	 == 0x46)
	printf("%s: Archivo ELF\n", argv[i]);
      else
	printf("%s: Archivo no identificado\n", argv[i]);
      break;
    case 0x89://PNG 89 50 4E 47 0D 0A 1A 0A
      if(bytesMagicos[1] == 0x50 && bytesMagicos[2] == 0x4E && bytesMagicos[3]
	 == 0x47 && bytesMagicos[4] == 0x0D && bytesMagicos[5] == 0x0A &&
	 bytesMagicos[6] == 0x1A && bytesMagicos[7] == 0x0A)
	printf("%s: Archivo PNG\n", argv[i]);
      else
	printf("%s: Archivo no identificado\n", argv[i]);
      break;
    case 0x50://ZIP 50 4B 03 04
      if(bytesMagicos[1] == 0x4B && bytesMagicos[2] == 0x03 && bytesMagicos[3]
	 == 0x04)
	printf("%s: Archivo ZIP\n", argv[i]);
      else
	printf("%s: Archivo no identificado\n", argv[i]);
      break;
    case 0x25://PDF 25 50 44 46 2d
      if(bytesMagicos[1] == 0x50 && bytesMagicos[2] == 0x44 && bytesMagicos[3]
	 == 0x46 && bytesMagicos[4] == 0x2d)
	printf("%s: Archivo PDF\n", argv[i]);
      else
	printf("%s: Archivo no identificado\n", argv[i]);
      break;
    case 0xFF://MP3 FF FB
      if(bytesMagicos[1] == 0xFB)
	printf("%s: Archivo MP3\n", argv[i]);
      else
	printf("%s: Archivo no identificado\n", argv[i]);
      break;
    case 0x49://MP3 49 44 33
      if(bytesMagicos[1] == 0x44 && bytesMagicos[2] == 0x33)
	printf("%s: Archivo MP3\n", argv[i]);
      else
	printf("%s: Archivo no identificado\n", argv[i]);
      break;
      case 0x4D://EXE 4D 5A
      if(bytesMagicos[1] == 0x5A)
	printf("%s: Archivo EXE\n", argv[i]);
      else
	printf("%s: Archivo no identificado\n", argv[i]);
      break;
    default:
      printf("%s: Archivo no identificado\n", argv[i]);
      break;
    }
  }
}
