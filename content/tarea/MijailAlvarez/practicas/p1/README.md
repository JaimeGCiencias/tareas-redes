# Práctica 1: Programación en C
## Alvarez Cerrillo Mijail Aron
### 310020590

Ejecutamos el makefile con make.

Programa Ejemplo
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p1/imagenes/1.png)


Programa Manejo Archivos
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p1/imagenes/3.png)

Añadir archivos
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p1/imagenes/4.png)

Leer archivo
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p1/imagenes/5.png)

Eliminar archivo
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p1/imagenes/6.png)

Burbuja
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p1/imagenes/7.png)
