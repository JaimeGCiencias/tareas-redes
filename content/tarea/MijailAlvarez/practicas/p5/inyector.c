// p5-ejercicio 1 Gomez Gutierrez & Alvarez Cerrillo
#include <stdio.h>
#include <stdlib.h>
#include <pcap.h>
#include <string.h>

int main(int argc, char *argv[]){

    if(argc <= 3){
      printf("salimos...\n");
      exit(0);
    }

    char trama[20];

    char *token = strtok(argv[2], ":");
    int dig = strtol(token, NULL, 16);
    trama[0]=dig;

    token = strtok(NULL, ":");
    dig = strtol(token, NULL, 16);
    trama[1]=dig;

    token = strtok(NULL, ":");
    dig = strtol(token, NULL, 16);
    trama[2]=dig;

    token = strtok(NULL, ":");
    dig = strtol(token, NULL, 16);
    trama[3]=dig;

    token = strtok(NULL, ":");
    dig = strtol(token, NULL, 16);
    trama[4]=dig;

    token = strtok(NULL, ":");
    dig = strtol(token, NULL, 16);
    trama[5]=dig;

    char *token1 = strtok(argv[3], ":");
    dig = strtol(token1, NULL, 16);
    trama[6]=dig;

    token1 = strtok(NULL, ":");
    dig = strtol(token1, NULL, 16);
    trama[7]=dig;

    token1 = strtok(NULL, ":");
    dig = strtol(token1, NULL, 16);
    trama[8]=dig;

    token1 = strtok(NULL, ":");
    dig = strtol(token1, NULL, 16);
    trama[9]=dig;

    token1 = strtok(NULL, ":");
    dig = strtol(token1, NULL, 16);
    trama[10]=dig;

    token1 = strtok(NULL, ":");
    dig = strtol(token1, NULL, 16);
    trama[11]=dig;

    char tipos[]= {0xab, 0xcd};
    memcpy(trama + 12, tipos, 2);
    char mensaje[] = "casado";
    memcpy(trama + 14, mensaje, sizeof(mensaje));

    char *interfaz = argv[1];
    pcap_t *open_live = pcap_open_live(interfaz, 1514, 0, 0, NULL);
    if (!open_live){
        printf("SALIENDO...\n");
        exit(1);
    }

    int bytes = pcap_inject(open_live, trama, sizeof(trama)) == -1;

    if (bytes == -1){
        pcap_perror(open_live, 0);
        pcap_close(open_live);
        exit(1);
    }else{
        printf("bytes enviados %i\n", bytes);
    }
    pcap_close(open_live);

    //printf("%s\n", trama);

    return 0;
}
