# Práctica 4
## Alvarez Cerrillo Mijail Aron
### 310020590

#### Creación red virtual
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p4/imagenes/p4-1.png)

#### Subred a red virtual
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p4/imagenes/p4-2.png)

#### Instalación Debian 9 on VMware
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p4/imagenes/p4-3.png)

#### Cambiamos el tipo de conexion de nuestra maquina virtual
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p4/imagenes/p4-4.png)

#### Agregamos una interfaz, reiniciamos la red y mostramos que se haya efectuado el cambio con "ip a".
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p4/imagenes/p4-5.png)

#### Probamos que tenemos conexion
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p4/imagenes/p4-6.png)

#### Creamos la nueva maquina virtual(cliente), le cambiamo la red a vmnet10 y revisamos que esten "enlazadas" con ip a.
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p4/imagenes/p4-7(cliente).png)

#### TCPDUMP en la maquina con el dhcp
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p4/imagenes/p4-8.png)

#### En servidor DHCP, observar que en la bitácora del sistema
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p4/imagenes/p4-9(cat-var-log-syslog).png)

#### Preguntas:

##### ¿Cómo funciona el protocolo DHCP?

El cliente DHCP solicita una dirección IP al transmitir un mensaje DHCPDiscover a la subred local. Al cliente se le ofrece una dirección cuando un servidor DHCP responde con un mensaje DHCPOffer que contiene una dirección IP e información de configuración para arrendar al cliente.[^1]

##### ¿Cómo se filtra el tráfico del protocolo DHCP en una captura de tráfico en Wireshark y en tcpdump?

El primer método para capturar el tráfico DHCP es usar la herramienta venerable tcpdump. En este caso, desea definir un filtro para que tcpdump vuelque solo el tráfico relacionado con DHCP. En DHCP, el número de puerto UDP 67 es usado por un servidor DHCP, y el número de puerto UDP 68 es usado por clientes DHCP.[^2]

##### ¿Qué es el estándar PXE y cuál es su utilidad?

Abreviatura de Pre-Boot Execution Environment. Pronunciado pixie(ingles), PXE es uno de los componentes de la especificación WfM de Intel. Permite que una estación de trabajo arranque desde un servidor en una red antes de iniciar el sistema operativo en el disco duro local.[^3]

[^1]: https://www.quora.com/How-does-DHCP-work

[^2]: http://ask.xmodulo.com/monitor-dhcp-traffic-command-line-linux.html

[^3]:https://www.webopedia.com/TERM/P/PXE.html
