#Universidad Nacional Autonoma de Mexico.

#Facultad de Ciencias & Ciencias de la Computacion.

## Redes de Computadoras 2019-2.
## Martinez Sanchez Jose Manuel.
### Practica 4.

Configurando la interfaz de red, editando el archivo /etc/resolv.conf
![s1](img/Captura_de_pantalla__1_.png)

Comprobando que la interfaz de la maquina Debian 1 posee IP asignada.
![s1](img/Captura_de_pantalla__2_.png)

Configurando el adaptador de red de la maquina debian 2.
![s1](img/Captura_de_pantalla__4_.png)

Comprobando que la maquina Debian 2 posee direccion IP asignada.
![s1](img/Captura_de_pantalla__5_.png)

Capturando trafico DCHP con tcpdump
![s1](img/Captura_de_pantalla__6_.png)

Observando el log /var/log/syslog
![s1](img/Captura_de_pantalla__7_.png)

Configurando maquina Debian 3
![s1](img/Captura_de_pantalla__8_.png)

Observando trafico DHCP para el arranque de la maquina Debian 3.
![s1](img/Captura_de_pantalla__9_.png)


¿Cómo funciona el protocolo DHCP?
El protocolo DHCP (en inglés: Dynamic Host Configuration Protocol)  es un protocolo de red, del tipo cliente servidor, que pertenece a la capa de aplicacion y opera en los puertos 67/UDP (servidor) y 68/UDP (cliente). Este protocolo permite configurar dinámicamente los parámetros esenciales TCP/IP de los hosts (estaciones de trabajo y servidores) de una red.  Funciona de la siguiente manera:

En primer lugar, podemos dividir la operacion del protocolo en dos grandes componentes que son:

  - Asignacion de direcciones IP y parametros TCP/IP.
  - Negociar y transmitir informacion especifica del host.


-Asignacion de direcciones IP y parametros TCP/IP.
Cuando un cliente ha contactado con un servidor DHCP, a través de varios estados internos, negocia el uso y la duración de su dirección IP.
La asignacion de direcciones IP y parametros puede realizarse de las siguientes maneras:
Asignación manual El administrador de red pone manualmente la dirección IP del cliente DHCP en el servidor DHCP. El DHCP se usa para dar al cliente DHCP el valor de esta dirección IP configurada manualmente.

Asignación automática No se requiere asignar manualmente direcciones IP. El servidor DHCP asigna al cliente DHCP, en el primer contacto, una dirección IP permanente que no podrá reutilizar ningún otro cliente DHCP.

Asignación dinámica El DHCP asigna una dirección IP al cliente DHCP por un tiempo determinado. Después que expire este lapso, se revoca la dirección IP y el cliente DHCP tiene que devolverla. Si el cliente aún necesita una dirección IP para efectuar sus operaciones, deberá solicitarla nuevamente.

La adquisision de estos parametros se realiza mediante las siguinetes etapas:

Descubrimiento de un servidor DHCP (SELECTING)

Cuando se inicializa el cliente DHCP, éste comienza en el estado de inicialización INIT. El cliente DHCP desconoce sus parámetros IP y por eso envía un broadcast DHCPDISCOVER. El mensaje DHCPDISCOVER se encapsula en un paquete UDP. Se coloca el número 67 con puerta de destino UDP, el mismo utilizado por el servidor BOOTP, debido a que el protocolo DHCP es una extensión de este protocolo. El mensaje DHCPDISCOVER usa la dirección IP de broadcast de valor 255. 255. 255. 255. Si no existe un servidor DHCP en la red local, el router IP debe tener un agente DHCP relay que soporte la retransmisión de esta petición hacia las otras subredes.

Antes de enviar el mensaje broadcast DHCPDISCOVER, el cliente DHCP espera por un tiempo aleatorio –entre 1 a 10 segundos– para evitar una colisión con otro cliente DHCP, como en el caso que todos los clientes DHCP se inicialicen al mismo tiempo al recibir todos energía a la vez (como una pérdida o interrupción de la electricidad).

Aceptación de la asignación recibida (REQUESTING)

Después de enviar el mensaje broadcast DHCPDISCOVER, el cliente DHCP ingresa al estado SELECTING, donde recibe los mensajes DHCPOFFER de los servidores DHCP configurados para atenderlo. El tiempo que el cliente DHCP esperará por los mensajes DHCPOFFER depende de la implementación. Si el cliente DHCP recibe varias respuestas DHCPOFFER, elegirá una. En reacción, el cliente DHCP enviará un mensaje DHCPREQUEST para elegir un servidor DHCP, el que contestará con un DHCPACK.

Como opción, el cliente DHCP controla la dirección IP enviada en el DHCPACK para verificar si está o no está en uso. En una red con broadcast, el cliente DHCP envía una petición ARP con la dirección IP sugerida para verificar que no esté duplicada. En caso de estarlo, el DHCPACK proveniente del servidor se ignora y se envía un DHCPDECLINE, con lo cual el cliente DHCP ingresa en estado INIT y vuelve a pedir una dirección IP válida que no esté en uso.

Cuando la petición ARP se difunde sobre la red, el cliente usa su propia dirección de hardware en el campo de dirección fuente de hardware del ARP, pero coloca el valor de 0 en el campo de dirección fuente IP. Esta dirección de valor 0 se utiliza en lugar de la dirección IP sugerida, para no confundir a las memorias caché ARP de otros hosts.

Renovación de la concesión (RENEWING)

Después de la expiración del temporizador T1, el cliente DHCP se mueve del estado BOUND al estado RENEWING (renovación) . En este último estado se debe negociar un nuevo alquiler para la dirección IP designada, entre el cliente DHCP y el servidor DHCP que originalmente le asignó la dirección IP. Si el servidor DHCP original, por algún motivo, no renueva el alquiler, le enviará un mensaje DHCPNACK y el cliente DHCP se moverá al estado INIT y intentará obtener una nueva dirección IP. En el caso contrario, si el servidor DHCP original envía un mensaje DHCPACK, éste contendrá la duración del nuevo alquiler. Entonces, el cliente DHCP coloca los valores de sus temporizadores y se moverá al estado BOUND.

Estado de reenganche (RENEWING)

Si el temporizador T2 (tiempo de reenganche) expira mientras el cliente DHCP está esperando en el estado RENEWING una respuesta sea DHCPACK o DHCPNACK proveniente del servidor DHCP original, el cliente DHCP se moverá al estado REBINDING. El servidor original DHCP podría no haber respondido porque estaría apagado o porque el enlace con la red habría caído. Nótese en las ecuaciones previas que T2 es mayor que T1, de modo que el cliente DHCP espera que el servidor original DHCP renueve el alquiler por un tiempo igual a T2 – T1.

Extensión de la concesión

Al expirar el temporizador T2 (tiempo de reenganche), el cliente DHCP enviará un DHCPREQUEST a la red para contactar con cualquier servidor DHCP para extender el alquiler, con lo cual pasará al estado REBINDING. El cliente DHCP envía este mensaje broadcast DHCPREQUEST porque presume que, luego de haber esperado T2 – T1 segundos en el estado RENEWING, el servidor DHCP original no está disponible, por lo cual tratará de contactar con otro servidor DHCP para que le responda. Si un servidor DHCP responde con un DHCPACK, el cliente DHCP renueva su alquiler (T3), coloca los temporizadores T1 y T2 y retorna al estado BOUND. Si no hay servidor DHCP disponible para renovar alquiler luego de expirar el temporizador T3, el alquiler cesa y el cliente DHCP pasa al estado INIT. Nótese que el cliente DHCP intentó renovar el alquiler primero con el servidor original y luego con cualquier otro servidor en la red.

Expiración de la concesión

Al acabar el alquiler (T3 expira), el cliente DHCP debe devolver su dirección IP y cesar toda acción con dicha dirección IP en la red. El cliente DHCP no siempre tiene que esperar la expiración del alquiler para terminar el uso de una dirección IP. Éste puede renunciar voluntariamente a una dirección IP, cancelando su alquiler. Por ejemplo, el usuario de un computador portátil podría conectarse a la red para una actividad particular. El servidor DHCP de la red podría colocar la dirección del alquiler por una hora. Suponiendo que el usuario acabe su tarea en 30 minutos, entonces se desconectará de la red al cabo de dicho lapso. Cuando el usuario se libera armoniosamente, el cliente DHCP enviará un mensaje DHCPRELEASE al servidor DHCP para cancelar el alquiler. La dirección IP ahora estará disponible.

Si los clientes DHCP operan en ordenadores que tienen disco duro, la dirección IP asignada puede ser almacenada en este dispositivo y, cuando la computadora reinicie sus operaciones, puede hacer una nueva petición usando esta dirección IP.

¿Cómo se filtra el tráfico del protocolo DHCP en una captura de tráfico en Wireshark y en tcpdump?

En Wireshark:

  Como sabemos que DHCP utiliza el puerto 67, podemos hacer: `udp.port == 67`

En tcpdump:

  tcpdump -i <interfaz de red> port 67 or port 68 -e -n

¿Qué es el estándar PXE y cuál es su utilidad?

El estandar PXE (Preboot eXecution Environment) es un entorno para arrancar e instalar el sistema operativo en computadoras a través de una red, de manera independiente de los dispositivos de almacenamiento de datos disponibles, consiste en una combinación de los protocolos DHCP y TFTP con pequeñas modificaciones en ambos. DHCP es utilizado para localizar el servidor de arranque apropiado, con TFTP se descarga el programa inicial de bootstrap y archivos adicionales.

Su utilidad radica en que permite el arranque del sistema operativo desde la red.
