/*
 * Redes de Computadoras 2019-2
 * Martinez Sanchez Jose Manuel
 * strings.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

 int main(int argc, char **argv){
     FILE *fp; 
    for (int i = 1; i < argc; ++i){
      fp = fopen(argv[i], "rb");
      unsigned short stringLength = 0;
      fread(&stringLength, sizeof(unsigned short), 1, fp);
      char *detectedStrings = malloc(sizeof(char) * stringLength);
      int count = fread(detectedStrings, sizeof(char), stringLength, fp);


      printf("%s\n", detectedStrings);
      fclose(fp);
    }


 }
/*
 * strings.c
 */
