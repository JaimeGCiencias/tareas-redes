#Universidad Nacional Autonoma de Mexico.

#Facultad de Ciencias & Ciencias de la Computacion.

## Redes de Computadoras 2019-2.
## Martinez Sanchez Jose Manuel.
### Practica 3.

En el directorio ```/src``` hacer ```make``` para compilar.

Ejemplos de ejecucion:

Numeros magicos:
![magic](/src/img/s1.png)

strings.c :
![magic](/src/img/s2.png)

Calculo de Hashes:
![magic](/src/img/s3.png)
