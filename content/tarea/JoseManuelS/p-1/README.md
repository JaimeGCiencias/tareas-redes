<<<<<<< HEAD
#Universidad Nacional Autonoma de Mexico.
=======
# Practica 1.
Martinez Sanchez Jose Manuel.
>>>>>>> 3e356e4ac435a387480704b8da480bb8edfa8ac4

#Facultad de Ciencias & Ciencias de la Computacion.

## Redes de Computadoras 2019-2.
## Martinez Sanchez Jose Manuel.
### Practica 2.

En el directorio ```/src``` hacer ```make``` para compilar.  Ejecutar con ```./archivos```.

Ejemplo de ejecucion:

Pantalla principal, agredando el archivo ```a1```
![Main Screen](/src/img/s1.png)


Leyendo archivo existente(Text.txt) a partir de su indice:
![Main Screen](/src/img/s2.png)


Eliminando un elemento de la lista, a partir de su indice.
![Main Screen](/src/img/s3.png)


Imprimiendo los elementos de la lista:
![Main Screen](/src/img/s4.png)
