#Universidad Nacional Autonoma de Mexico.

#Facultad de Ciencias & Ciencias de la Computacion.

## Redes de Computadoras 2019-2.
## Martinez Sanchez Jose Manuel.
### Practica 5.

Agregando el subdominio creado:
![s1](img/s1.png)

Ingresando los servidores DNS:
![s3](img/s3.png)

Creando registro tipo A:
![s4](img/s4.png)

Comprobando con nslookup:
![s5](img/s5.png)






¿Qué es un servidor DNS?

Es un servidor que traduce un identificador basado en texto a una identificación numérica en respuesta a la peticion de un cliente.


¿Para qué se utiliza cada uno de los registros SOA, NS, A, AAAA, PTR, CNAME, MX y TXT?

    SOA: muestra las características básicas del dominio y de la zona en la que se encuentra. Contiene la siguiente informacion:
    
        MNAME: Nombre de dominio del servidor DNS constituido como servidor primario para la zona.
        
        RNAME: Nombre de dominio que indica la dirección de correo de la persona responsable de la zona.

        SERIAL: Número entero de 32 bits correspondiente a la copia original de la zona. Este valor se incrementa con cada actualización, se conserva en las transferencias de zona, y puede ser utilizado como verificación.
        
        REFRESH: Número de 32 bits representando el intervalo de tiempo antes que la zona deba ser actualizada (Refresco).
        
        RETRY: Número de 32 bits representando el intervalo de tiempo que debe consentirse antes de establecer que una petición de actualización ha fallado (Reintento).
        
        EXPIRE: Número de 32 bits que especifica el límite máximo de tiempo que puede transcurrir antes que la zona deje de ser “autoridad”. (Caducidad)
        
        MINIMUM: Número entero de 32 bits señalando el valor mínimo del parámetro TTL (Time To Live) que debe ser utilizado para cualquier exploración de la zona. (Tiempo de vida mínimo)

    NS: Contiene los servidores de nombre de ese dominio, lo que permite que otros servidores de nombres vean los nombres de su dominio.
    
    A: Los registros de dirección A, (Adress) asocian nombres de host a direcciones IPv4  dentro de una zona. Son los más numerosos dentro del archivo.

    AAAA: Asocian nombres de host a direcciones IPv6  dentro de una zona.
    
    PTR: Define las direcciones IP de todos los sistemas en una notación invertida. Esta inversión permite que se pueda buscar una IP en el DNS​ ya que a la notación de la IP invertida se le añade el dominio in-addr.arpa, convirtiendo la IP en un nombre de dominio.
    
    CNAME:  Tipo de registro DNS que asigna un alias a un nombre de dominio auténtico o canónico.
    
    MX: Especifica cómo debe ser encaminado un correo electrónico en internet. Los registros MX apuntan a los servidores a los cuales envían un correo electrónico, y a cuál de ellos debería ser enviado en primer lugar, por prioridad.
    
    TXT: Es un tipo de registro de sistema de nombres de dominio (DNS) que contiene información de texto de fuentes externas a un dominio y que se añade a la configuración de este.
¿Qué hacen las herramientas nslookup y dig?

    nslookup: Es una herramienta que nos permite saber si el DNS está resolviendo correctamente los nombres y las IPs. Resulver IPs a partir de nombres y visceversa. 
    
    dig: Es una herramienta que nos permite realizar consultas a registros DNS. Su sintaxis es: dig @IP dominio tipo

Agregando el subdominio creado:
![s1](img/s1.png)

Configurando servidor DHCP:
![pt2](img/pt2.png)

Configurando servidor DNS:
![pt4](img/pt4.png)

Verificanco conexion entre PC1 y PC2:
![pt1](img/pt1.png)

Comprobando la conectividad del servidor redes.net:
![pt5](img/pt5.png)

Observando tabla de traducciones del router 2:
![pt9](img/pt9.png)


Comprobando la conectividad de PC1:
![pt7](img/pt7.png)

Comprobando la conectividad de PC3:
![pt8](img/pt8.png)
    
¿Qué diferencia hay entre un switch y un router?

La diferencia entre estos dos equipos radica en que el switch solo transfiere los paquetes de manera diferencial entre un equipo y otro, mientras que el router, ademas de disponer de esas funciones, permite dirigir un paquete entre distintas redes para hacerlo llegar a su destino final.

¿Qué tipos de switches hay?
    Cut through
    Desde el punto de vista de la innovación tecnológica, se sabe que estos tipos de switch son una mejora de la versión denominada “store and forward”; los cuales tienen el mecanismo central de procesar los primeros bytes de información que se lograban comunicar. Para esto se emplea un criterio la fuente de origen de la información contenida en los bytes, con la finalidad de evitar errores de datos.

    Cut through adaptivo
    Es un tipo de switch que permite una adaptación para que de acuerdo a las necesidades del usuario, pueda trabajar tanto para un “cut throught” o un “store and forward”; y esto dependerá de la cantidad y calidad de información que se considera transferir. Es por ese beneficio de depurar con mayor eficacia los datos erróneos que se pudieran transferir, que se usa con mucha frecuencia.
    
    Store and forward
    En este caso, tenemos que funciona de la siguiente manera: guarda los datos en un sistema buffer para que esta proceda a analizar y medir el contenido de la información, para así poder eliminar los datos que considere son incorrectos. Gracias a este mecanismo es posible transferir información mucho más rápida y con los datos que realmente se necesitan y son viables.

    Swith apilables
    La característica principal de este tipo, es que cuentan con el mecanismo de usar en su hardware, de un bus de expansión; es precisamente en este dispositivo que es posible insertarse varias unidades de switch para optimizar más este recurso. El beneficio inmediato que genera esta estrategia es que es posible incrementar el ancho de banda, lo que hará que sea más eficiente la comunicación.
    
    Switch no apilable
    En este caso, tal vez porque las condiciones del hardware o de la factibilidad de la comunicación así lo permiten, no es necesario que se emplee el llamado bus que es parte de un dispositivo de comunicación dentro del hardware, para que de manera estratégica se añadan otros switch. De tal manera que no existirá más que el switch necesario.
    
    Switch desktop
    Este tipo de switch suele ser conocido como el más común de todos los hasta ahora existentes. Lo que ofrece es una forma de comunicación interconectada básica. Generalmente se emplean en los hogares como una forma de comunicación de carácter  precisamente, doméstico. Suele emplearse también en empresas pequeñas para no generar una fuerte inversión.
    
    Switch gestionable
    Este es un tipo de switch que tiene la función principal de poder darle al usuario la posibilidad de manejar mediante un proceso de gestión, la manera en cómo se procesará la información dentro del sistema de red de comunicación interno en la organización. Para esto es necesario configurar este dispositivo de acuerdo a las características particulares.
    
    Swith no gestionable
    Son principalmente aquellos dispositivos que desde su origen, solo ofrecen las funciones y características básicas de este tipo. Es por esa razón que cuando se instalan y se empiezan a usar, no es necesario que se llegue a un proceso de configuración, simplemente se instala el dispositivo y ya. Lo ideal sería contar con mecanismos de manejo de datos aunque sin ellos es posible intercambiar datos.
    
    Switch perimetral
    Se denominan como tal, precisamente a aquellos switch que tienen la característica y función principal de poder conectarse a lo que se conoce como “red troncal”, dentro de todo el componente del hardware en un conmutador. Por ese funcionamiento en los niveles de comunicación es que se debe de determinar por el usuario en qué nivel de red se hará esa conexión, es decir, es jerárquico.
    
    Switches perimetrales gestionables
    Se considera el uso de este tipo cuando la organización es de tipo mediano o grande en cuanto a tamaño y necesidades de comunicación interna. Para tal fin, es necesario que se establezca una jerarquización en el uso de la red para realizar la interconexión por prioridades. Esto permite optimizar la red de comunicación y el uso eficiente de los recursos del hardware que se ocupa.
    
    Switches perimetrales no gestionables
    Son ideales para implementarse cuando las necesidades de comunicación son pequeñas. En cierto modo, guardan cierto parecido tanto en características como funciones, con el switch desktop; esto es así, ya que no permite realizar una configuración para optimizar el funcionamiento del dispositivo. La diferencia central entre este y el de tipo desktop está en que tiene mayor número de puertos.
    
    Switch troncal
    De manera puntual, estos llegan a emplearse en un sistema de núcleo bajo un sistema de redes de ampliación. Esto permite al usuario y de acuerdo a sus propias necesidades de comunicación, conectar otros switches, routers e incluso servidores. Esto se lleva a cabo con fines de mejora continua en la comunicación interna dentro de la organización que en ese momento lo está usando.
    
    
¿Qué es una lista de acceso y para qué se utilizan?

Una lista de control de acceso es un mecanismo para determinar los permisos de acceso apropiados a un entidad especifica,  permiten controlar el flujo del tráfico en equipos de redes, tales como enrutadores y conmutadores. Su principal objetivo es filtrar tráfico, permitiendo o denegando el tráfico de red de acuerdo a alguna condición.



Se creó una lista de acceso con el comando access-list 1 permit 192.168.1.0 0.0.0.255, la última parte del comando (0.0.0.255) es una wildcard, ¿qué es una wildcard  y en qué se diferencia con una máscara de red?


Una wildcard  una máscara de bits que indica qué partes de una dirección de IP son relevantes para la ejecución de una determinada acción, sirve para indicar qué direcciones IP tendrían que ser permitidas o denegadas en las listas de control del acceso.
UNa wildcard se diferencia de una  máscara de red en el que lwildcard es una  máscara de red invertida y nos indica el numero de Host que en una IP, mientras que mascara de red nos dice el numero de Redes que hay en una IP.


¿Qué diferencia hay entre una dirección IP pública y una privada?

La direfecnia es que ;a IP pública es el identificador de nuestra red desde el exterior, es decir, la de nuestro router de casa, que es el que es visible desde fuera, mientras que la privada es la que identifica a cada uno de los dispositivos conectados a nuestra red, por lo tanto, cada una de las direcciones IP que el router asigna a nuestro ordenador, móvil, tablet o cualquier otro dispositivo que se esté conectado a él.


¿Qué diferencia hay entre NAT estática, NAT dinámica y PAT? ¿Cuál es la que se encuentra usualmente en los routers de los hogares?

NAT estatica: Es un tipo de NAT en el cuál se asigna una dirección IP privada con una dirección IP pública de forma estática.

NAT Dinamica: Es un tipo de NAT que utiliza un pool de IPs públicas para un pool de IPs privadas que serán mapeadas de forma dinámica y a demanda.

PAT: Es un mecanismo de asignación estática de puertos públicos a puertos y IP privadas, consiste en abrir un puerto de la parte externa del NAT y asignarlo a un host y un puerto interno. La principal diferencia con NAT es que un puerto externo siempre estará destinado al mismo host y puerto interno hasta que se cambie la configuración.



