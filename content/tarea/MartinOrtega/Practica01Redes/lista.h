
struct nodo{
    void * elemento;
    struct nodo * siguiente;
};

struct  lista
{
    struct nodo * cabeza;
    int longitud;
};



//Funcion que agrega un elemento a el final de la lsita
//Recibe una estructora lista y un elemento
void * agrega_elemento(struct lista * lista, void * elemento){
    struct nodo * nuevo;
    nuevo = malloc(sizeof(nuevo));
    nuevo -> elemento = elemento;

    struct nodo * aux;
    aux =  lista -> cabeza;
    if(lista->longitud != 0 && lista ->cabeza != 0){        
        while(aux->siguiente != 0){
            aux = aux -> siguiente;
        
            }
        aux ->siguiente = nuevo;
        lista -> longitud++;
    }
    else{
        lista -> cabeza = nuevo;
        lista -> longitud++;
    }

}
//Funcion que obtiene el n-esimo elemento de la lista empezando desde 0.
//Recibe una estructura lista y la n-enesima posicion.

void * obten_elemento(struct lista * lista, int n){
   int i = 0;
   int * a = &i;
    struct nodo * aux;
    aux = lista ->cabeza;
    if(n == 0 && lista->longitud != 0){
        return aux -> elemento;
    }
    if(aux -> siguiente != 0){
        while(i != n){
            aux = aux -> siguiente;
            i++;
        }
        return aux -> elemento;
    }else{
        return a;
    } 
}

//Funcion que elimina el n-enesimo elemento de la lista empezando desde 0.
//Recibe una lista y la posicion n-enesima de un elemento.
void * elimina_elemento(struct lista * lista, int n){
    
    int i = 0;
    struct nodo * aux;
    struct nodo * eliminado;
    aux = lista -> cabeza;
    if(lista->longitud != 0 && lista->longitud >= n){
        if (n == 0) {
            eliminado = aux->elemento;
            lista -> cabeza = aux -> siguiente;
            lista -> longitud--;
        } else {
            if (n == (lista -> longitud -1)){
                while(aux->siguiente->siguiente != 0){
                    aux = aux -> siguiente;
                    i++;
                }
                eliminado = aux -> siguiente->elemento;
                //aux -> siguiente = NULL;
                lista -> longitud--;
            } else {
                while(i < (n - 1)){
                    aux = aux -> siguiente;
                    i++;
                }
                eliminado = aux -> siguiente->elemento;
                aux -> siguiente = aux -> siguiente -> siguiente;
                lista -> longitud--;
            }
        }
    }
    //free(eliminado);
    return eliminado;
}


void aplica_funcion(struct lista * lista, void (*f)(void *)){
   
    struct nodo * aux;
    aux = lista->cabeza;
    while(aux->siguiente != 0){
        
        f(aux->elemento);
        aux = aux ->siguiente;
    }
    f(aux->elemento);
    
    
}