#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#define TAMANO_MAX_MSJ 65535

char buffersote[TAMANO_MAX_MSJ];
void error(char *mensaje)
{
    fprintf(stderr, "%s \n", mensaje);
    exit(1);
}

int unpack(unsigned char *bytes)
{
    return (bytes[0] << 8) + bytes[1];
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        fprintf(stderr, "Uso: %s <direccion IP> <puerto>\n", argv[0]);
        return 1;
    }
    int sockfd;
    struct sockaddr_in host_addr;
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        error("Error al crear socket \n");
    memset(&host_addr, 0, sizeof(host_addr));
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(atoi(argv[2]));
    if (inet_pton(AF_INET, argv[1], &host_addr.sin_addr) <= 0)
        error("Error al leer direccion IP de servidor \n");
    if (connect(sockfd, (struct sockaddr *)&host_addr, sizeof(host_addr)) < 0)
        error("Error al conectarse a servidor \n");
    char comando[256];
    while (1) {
        memset(comando, 0, 256);
        printf("> ");
        fgets(comando, 100, stdin);
        send(sockfd, comando, strlen(comando) - 1, 0);
        char bytes_tamano[2];
        recv(sockfd, bytes_tamano, 2, 0);
        int tamano = unpack(bytes_tamano);
        recv(sockfd, buffersote, tamano, 0);
        buffersote[tamano] = '\0';
        if (buffersote[0] == 0) {
            printf("Adios dijo el servidor\n");
            break;
        }
        printf("%s\n", buffersote);
    }
    close(sockfd);
    return 0;
}