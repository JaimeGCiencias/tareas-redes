#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#define TAMANO_MAX_MSJ 65535

char buffersote[TAMANO_MAX_MSJ];

void error(char *mensaje)
{
    fprintf(stderr, "%s \n", mensaje);
    exit(1);
}

unsigned char *pack(unsigned short numero)
{
    unsigned char *a = malloc(2);
    a[0] = (numero >> 8) & 0xff;
    a[1] = numero & 0xff;
    return a;
}

int copia_stream(FILE *flujo)
{
    int n = -1;
    char c;
    do
    {
        c = fgetc(flujo);
        buffersote[++n] = c;
    }
    while (c != EOF && n <= TAMANO_MAX_MSJ);
    buffersote[n] = '\0';
    return n;
}

int send_all(int socket, void *buffer, size_t length)
{
    char *ptr = (char *) buffer;
    while (length > 0)
    {
        int i = send(socket, ptr, length, 0);
        if (i < 1) return 0;
        ptr += i;
        length -= i;
    }
    return 1;
}

void *maneja_cliente(void *entrada)
{
    char comando[256];
    int cliente_sockfd = *(int *)entrada;
    while (1)
    {
        memset(comando, 0, 256);
        int bytes_recibidos = recv(cliente_sockfd, comando, 256, 0);
        comando[bytes_recibidos] = '\0';
        if (!strcmp(comando, "salir"))
        {
            char salir[] = {0, 1, 0};
            send_all(cliente_sockfd, salir, 3);
            break;
        }
        char *para_stderr = " 2>&1";
        strcat(comando, para_stderr);
        FILE *salida_comando = popen(comando, "r");
        int tamano = copia_stream(salida_comando);
        send_all(cliente_sockfd, pack(tamano), 2);
        send_all(cliente_sockfd, buffersote, tamano);
        pclose(salida_comando);
    }
    close(cliente_sockfd);
    printf("Termino cliente\n");
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Uso: %s <puerto>\n", argv[0]);
        return 1;
    }
    int sockfd, cliente_sockfd, opt = 1;
    struct sockaddr_in host_addr, cliente_addr ;
    socklen_t sin_tam = sizeof(struct sockaddr_in);
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        error("Error al crear el socket");
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, & opt, sizeof(int)) < 0)
        error("Error en setsockopt \n");
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(atoi(argv[1]));
    host_addr.sin_addr.s_addr = INADDR_ANY;
    if (bind(sockfd, (struct sockaddr *)&host_addr, sizeof(struct sockaddr)) < 0)
        error("Error en bind \n");
    if (listen(sockfd, 5) < 0)
        error("Error en listen \n");
    while (1)
    {
        cliente_sockfd = accept(sockfd,
                                (struct sockaddr *)&cliente_addr,
                                &sin_tam );
        if (cliente_sockfd < 0)
            error("Error en accept \n");
        printf("Conexion aceptada desde %s: %d \n",
               inet_ntoa(cliente_addr.sin_addr),
               ntohs(cliente_addr.sin_port));
        pthread_t *hilo = (pthread_t *)malloc(sizeof(pthread_t));
        pthread_create(hilo, NULL, maneja_cliente, &cliente_sockfd);
    }
    return 0;
}