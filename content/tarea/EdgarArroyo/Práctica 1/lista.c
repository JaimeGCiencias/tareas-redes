#include <stdlib.h>
#include <stdio.h>
#include "lista.h"

void imp(struct lista *lista)
{
    struct nodo *nodo = lista->cabeza;
    if (nodo)
        while (nodo)
        {
            printf("%c, ", *(int *)nodo->elemento);
            nodo = nodo->siguiente;
        }
    printf("\n");
}

void agrega_elemento(struct lista *lista, void *elemento)
{
    struct nodo *nuevo = (struct nodo *)malloc(sizeof(struct nodo));
    nuevo->elemento = elemento;
    nuevo->siguiente = NULL;
    struct nodo *nodo = lista->cabeza;
    if (!nodo)
    {
        lista->cabeza = nuevo;
    }
    else
    {
        while (nodo->siguiente)
        {
            nodo = nodo->siguiente;
        }
        nodo->siguiente = nuevo;
    }
    lista->longitud++;
}

void *obten_elemento(struct lista *lista, int n)
{
    if (n >= lista->longitud || n < 0)
    {
        int *cero = malloc(sizeof(int));
        *cero = 0;
        return cero;
    }
    struct nodo *nodo = lista->cabeza;
    while (n--)
    {
        nodo = nodo->siguiente;
    }
    return nodo->elemento;
}

void *elimina_elemento(struct lista *lista, int n)
{
    if (n >= lista->longitud || n < 0)
    {
        int *cero = malloc(sizeof(int));
        *cero = 0;
        return cero;
    }
    struct nodo *actual = lista->cabeza;
    struct nodo *anterior = NULL;
    while (n--)
    {
        anterior = actual;
        actual = actual->siguiente;
    }
    if (!anterior)
        lista->cabeza = actual->siguiente;
    else
        anterior->siguiente = actual->siguiente;
    lista->longitud--;
    return actual->elemento;
}

void aplica_funcion(struct lista *lista, void (*f)(void *))
{
    struct nodo *nodo = lista->cabeza;
    while (nodo)
    {
        (*f)(nodo->elemento);
        nodo = nodo->siguiente;
    }
}

