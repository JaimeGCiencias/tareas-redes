/* Para leer de /etc/passwd */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Uso: %s <archivo passwd>\n", argv[0]);
        exit(1);
    }

    const char *const campos[] = {"Nombre de usuario", "Contraseña", "UID", "GID",
                               "GECOS", "Home", "Shell"};
    const char *en_shadow = "Contraseña en /etc/shadow";
    const char *sin_contra = "Sin contraseña";
    const char *a;
    char *b;
    unsigned char linea[256];
    FILE *archivo = fopen(argv[1], "r");
    if (!archivo)
    {
        fprintf(stderr, "Error al abrir el archivo\n");
        exit(1);
    }

    while(fgets(linea, 256, archivo))
    {
        linea[strlen(linea) - 1] = '\0';
        b = strdup(linea);
        for (int i = 0; i < 7; i++)
        {
            a = strsep(&b, ":");
            if (i == 1)
            {
                if (!strcmp("x", a))
                    a = en_shadow;
                if (!strcmp("", a))
                    a = sin_contra;
            }
            printf("%s: %s\n", campos[i], a);
        }
        printf("\n");
    }
    fclose(archivo);
}