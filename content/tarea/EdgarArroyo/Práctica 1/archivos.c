#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "lista.h"
#define TAM_MAX_NOMBRE 100

void imprime(void *elemento)
{
    char *e = (char *)elemento;
    printf("%s, ", e);
}

int main(int argc, char const *argv[])
{
    struct lista *lista = (struct lista *)malloc(sizeof(struct lista));
    lista->cabeza = NULL;
    lista->longitud = 0;
    char opcion[3];
    char nombre[TAM_MAX_NOMBRE];
    char indice[3];
    int n;
    unsigned char uno[1];
    for (;;)
    {
        printf("\n 1. Insertar \n 2. Leer \n 3. Eliminar \n 4. Imprimir \n\n");
        fgets(opcion, 3, stdin);
        int a = strtol(opcion, NULL, 10);
        switch (a)
        {
        case 1:
            printf("Nombre de archivo nuevo\n");
            fgets(nombre, TAM_MAX_NOMBRE, stdin);
            if ((strlen(nombre) > 0) && (nombre[strlen (nombre) - 1] == '\n'))
                nombre[strlen (nombre) - 1] = '\0';
            char *nueva = malloc(strlen(nombre));
            strcpy(nueva, nombre);
            agrega_elemento(lista, nueva);
            break;

        case 2:
            printf("Introduce el índice\n");
            fgets(indice, 3, stdin);
            n = strtol(indice, NULL, 10);
            char *nombre_archivo = (char *)obten_elemento(lista, n);
            if (!*nombre_archivo)
                printf("Índice no válido\n");
            else
            {
                FILE *archivo = fopen(nombre_archivo, "r");
                if (!archivo)
                    printf("No existe %s\n", nombre_archivo);
                else
                {
                    printf("Contenido de %s:\n\n", nombre_archivo);
                    while (fread(uno, 1, 1, archivo))
                        printf("%c", *uno);
                    fclose(archivo);
                    printf("\n");
                }
            }
            break;

        case 3:
            printf("Introduce el índice\n");
            fgets(indice, 3, stdin);
            n = strtol(indice, NULL, 10);
            char *eliminado = (char *)elimina_elemento(lista, n);
            if (!*eliminado)
                printf("No se eliminó nada\n");
            else
                printf("Se eliminó el nombre %s\n", eliminado);
            break;

        case 4:
            printf("Archivos: ");
            aplica_funcion(lista, imprime);
            printf("\n");
            break;

        default:
            break;
        }
    }
    return 0;
}