## 1. Magia
![](imgs/magia.png)

## 2. Strings
![](imgs/strings.png)

## 3. Hashes
![](imgs/hash.png)

## 4. Filtros
Mostrar paquetes que tengan por dirección MAC origen 60:a4:4c:89:3c:a4 o que tengan por dirección MAC destino fc:fb:fb:01:fa:21.
`(tcpdump) ether src 60:a4:4c:89:3c:a4 or ether dst fc:fb:fb:01:fa:21`
`(Wireshark) eth.src == 60:a4:4c:89:3c:a4 or eth.dst == fc:fb:fb:01:fa:21`

Mostrar paqutes cuya dirección MAC ya sea origen o destino comience con 88:53:95.
`(tcpdump) ether[0:4] & 0xffffff00 = 0x88539500 or ether[6:4] & 0xffffff00 = 0x88539500`
`(Wireshark) eth.addr[0:3] == 88:53:95`

Mostrar paquetes cuyo ethertype sea IPv6.
`(tcpdump) ip6`
`(Wireshark) ipv6`

Mostrar paqutes que tengan por ethertype a ARP y cuya dirección MAC destino sea ff:ff:ff:ff:ff:ff.
`(tcpdump) arp and ether dst ff:ff:ff:ff:ff:ff`
`(Wireshark) arp and eth.dst == ff:ff:ff:ff:ff:ff`

Mostrar paquetes cuya dirección IP empieza con 172.
`(tcpdump) net 121.0.0.0/8`
`(Wireshark) ip.addr == 121.0.0.0/8`


## 5. Extra
- Direcciones IP: `172.16.16.1` y `172.16.16.217`
- Direcciones MAC de equipos: `00:50:56:c0:00:08` y `00:0c:29:79:af:e0`
- MD5 de archivo: `6fe658c629bcb4327d6c475e06d899fc`
- Usuario: `david`
- Contraseña: `blackstar`
De la captura leí las líneas de texto con el programa `strings`, con lo que se mostraban mensajes de una conversación. Ahí aparecía la contraseña para un archivo junto con su valor MD5. Luego aparecía una solicitud HTTP con dirección /secreto. En Wireshark extraje el contenido del archivo, quité el encabezado de HTTP y obtuve un archivo ZIP (cuyo MD5 es el ya mencionado), que pude extraer con la contraseña `hola123`. Solo contenía un archivo y este parece contener bytes de relleno además de la cadena que contiene un user y un password, que también pude ver con `strings`.
![](imgs/captura.png)
![](imgs/secreto.png)