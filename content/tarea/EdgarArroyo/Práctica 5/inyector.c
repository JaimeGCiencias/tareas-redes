#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pcap.h>

int main(int argc, char const *argv[])
{
    if (argc <= 3)
    {
        fprintf(stderr, "uso: inyector <interfaz> <MAC destino> <MAC origen>\n");
        exit(1);
    }

    const char *interfaz = argv[1];
    unsigned char mac_destino[6];
    unsigned char mac_origen[6];

    sscanf(argv[2], "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &mac_destino[0],
        &mac_destino[1], &mac_destino[2], &mac_destino[3], &mac_destino[4],
        &mac_destino[5]);
    sscanf(argv[3], "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &mac_origen[0],
        &mac_origen[1], &mac_origen[2], &mac_origen[3], &mac_origen[4],
        &mac_origen[5]);
    unsigned char tipo[] = {0xab, 0xcd};
    unsigned char payload[] = "CUALQUIER COSA";
    unsigned char trama[6 + 6 + 2 + sizeof(payload)];
    memcpy(trama, mac_destino, 6);
    memcpy(trama + 6, mac_origen, 6);
    memcpy(trama + 12, tipo, 2);
    memcpy(trama + 14, payload, sizeof(payload));

    char pcap_errbuf[PCAP_ERRBUF_SIZE];
    pcap_errbuf[0] = '\0';
    pcap_t *pcap = pcap_open_live(interfaz, 96, 0, 0, pcap_errbuf);
    if (pcap_errbuf[0] != '\0')
    {
        fprintf(stderr, "%s\n", pcap_errbuf);
    }
    if (!pcap)
    {
        exit(1);
    }
    if (pcap_inject(pcap, trama, sizeof(trama)) == -1)
    {
        pcap_perror(pcap, 0);
        pcap_close(pcap);
        exit(1);
    }
    pcap_close(pcap);
    return 0;
}