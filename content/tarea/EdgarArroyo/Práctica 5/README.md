## Inyección de tramas
![](imgs/inyector.png)

## DNS
![](imgs/dns_1.png)

![](imgs/dns_2.png)

![](imgs/dns_3.png)

![](imgs/dns_4.png)

![](imgs/dns_5.png)

![](imgs/dns_6.png)


#### ¿Qué es un servidor DNS?
Un servidor que almacena una lista de registros de la forma
(Nombre, Valor, Tipo),
que sirven para transformar una dirección (dominio) en otra dirección (una dirección IP o un dominio distinto, por ejemplo). Normalmente usan el software BIND y ocupan el puerto 53 para recibir peticiones.

#### ¿Para qué se utiliza cada uno de los registros SOA, NS, A, AAAA, PTR, CNAME, MX y TXT?
Cada tipo de registro sirve para indicar el "tipo de traducción" que está haciendo el servidor DNS.
Tipo A. El campo Nombre es el hostname y su valor es la dirección IPv4.
Tipo NS. El valor es la dirección del Authoritative Name Server encargado de una zona DNS, por ejemplo, los .com.
Tipo AAAA. El valor es la dirección IPv6. 
Tipo SOA. Parecido al NS pero con más información sobre "servidor autoridad" encargado de una zona DNS.
Tipo CNAME. El valor es otro hostname (otro alias).
Tipo MX. El nombre es el hostname y el valor es una dirección del servidor de email.
Tipo PTR. Sirve para hacer una "traducción inversa", el nombre sería una dirección IP y el valor un dominio.

#### ¿Qué hacen las herramientas nslookup y dig?
nslookup sirve para manualmente solicitar un registro a un servidor DNS.
dig también sirve para consultar servidores DNS pero es más flexible.


## Packet Tracer
Se incluye archivo red.pkt

![](imgs/pt_1.png)

![](imgs/pt_2.png)

![](imgs/pt_3.png)

![](imgs/pt_4.png)

![](imgs/pt_5.png)

![](imgs/pt_6.png)

![](imgs/pt_7.png)

![](imgs/pt_8.png)

![](imgs/pt_9.png)

![](imgs/pt_10.png)

![](imgs/pt_11.png)

![](imgs/pt_12.png)

![](imgs/pt_13.png)

![](imgs/pt_14.png)

![](imgs/pt_15.png)

#### ¿Qué diferencia hay entre un switch y un router?
Comúnmente un switch no incluye funcionalidad de la capa de red (algoritmos de ruteo), solamente sirve para conectar distintos hosts en una red. En cambio, la función principal de un router, aunque también incluye "switching", involucra el control hecho por los algoritmos y tablas de ruteo.

#### ¿Qué tipos de switches hay?
#### ¿Qué es una lista de acceso y para qué se utilizan?
#### Se creó una lista de acceso con el comando access-list 1 permit 192.168.1.0 0.0.0.255, la última parte del comando (0.0.0.255) es una wildcard, ¿qué es una wildcard  y en qué se diferencia con una máscara de red?
#### ¿Qué diferencia hay entre una dirección IP pública y una privada?
#### ¿Qué diferencia hay entre NAT estática, NAT dinámica y PAT? ¿Cuál es la que se encuentra usualmente en los routers de los hogares?