/* Estructura para representar a un nodo */
struct nodo {
   void * elemento;
   struct nodo * siguiente;
};

/* Estructura para representar a una lista */
struct lista {
   struct nodo * cabeza;
   int longitud;
};

/* Agrega un elemento al final de la lista. */
void agrega_elemento(struct lista * lista, void * elemento);

/* Permite obtener el n-elemento de la lista, empezando desde 0. */
void * obten_elemento(struct lista * lista, int n);

/* Elimina el n-elemento de la lista, empezando desde 0. */
void * elimina_elemento(struct lista * lista, int n);

/* Aplica la función f a todos los elementos de la lista. */
void aplica_funcion(struct lista * lista, void (*f)(void *));
