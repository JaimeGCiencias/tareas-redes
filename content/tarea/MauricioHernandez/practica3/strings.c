#include <stdio.h>

int check_file(char c[]);

int main(int argc, char *argv[]){

  // Primer argumento es el nombre del ejecutable
  // Entonces empezamos desde el segundo argumento
  if(argc != 2){
    fprintf(stderr, "Se espera  sólo un archivo de entrada\n");
    return 1;
  }

  // Checamos el archivo
  return check_file(argv[1]);

}

int check_file(char filename[]){

  FILE *f;

  if( (f = fopen(filename, "rb")) == NULL){
    fprintf(stderr, "No pude abrir el archivo con nombre %s\n", filename);
    return 1;
  }

  int c;
  while ((c = fgetc(f)) != EOF){
    if ((32 <= c) && (c <= 126))
      printf("%c", c);
  }

  return 0;
}
