// #DEFINE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int check_file(char c[]);

int main(int argc, char *argv[]){

  // Primer argumento es el nombre del ejecutable
  // Entonces empezamos desde el segundo argumento
  for(int i = 1; i < argc; i++){
    check_file(argv[i]);
  }
  return 0;
}

void DecToHexStr(int dec, char *str){
  sprintf(str, "%X", dec);
}

int check_file(char filename[]){
  char **bytes;
  bytes = malloc(8 * sizeof(char *));

  for(int i = 0; i < 8; i++){
    bytes[i] = malloc(sizeof(char) + 1);
  }
  // char *bytes[8];

  // printf("%lu\n", sizeof(bytes));

  FILE *f;

  if( (f = fopen(filename, "rb")) == NULL){
    fprintf(stderr, "No pude abrir el archivo con nombre %s\n", filename);
    return 1;
  }

  for (int i = 0; i < 8; i++){
    DecToHexStr(fgetc(f), bytes[i]);
  }

  // Vamos a checar el arreglo bytes
  // GIF
  if (strstr(bytes[0], "47") != NULL &&
      strstr(bytes[1], "49") != NULL &&
      strstr(bytes[2], "46") != NULL &&
      strstr(bytes[3], "38") != NULL &&
      (strstr(bytes[4], "37") != NULL || strstr(bytes[4], "39") != NULL) &&
      strstr(bytes[5], "61") != NULL)
    printf("Es un GIF\n");
  // ELF
  else if(strstr(bytes[0], "7F") != NULL &&
      strstr(bytes[1], "45") != NULL &&
      strstr(bytes[2], "4C") != NULL &&
      strstr(bytes[3], "46") != NULL)
    printf("Es un ELF\n");
  // PNG
  else if(strstr(bytes[0], "89") != NULL &&
      strstr(bytes[1], "50") != NULL &&
      strstr(bytes[2], "4E") != NULL &&
      strstr(bytes[3], "47") != NULL &&
      strstr(bytes[4], "D") != NULL &&
      strstr(bytes[5], "A") != NULL &&
      strstr(bytes[6], "1A") != NULL &&
      strstr(bytes[7], "A") != NULL)
    printf("Es un PNG\n");
  // ZIP
  else if(strstr(bytes[0], "50") != NULL &&
      strstr(bytes[1], "4B") != NULL &&
      (strstr(bytes[2], "3") != NULL || strstr(bytes[2], "5") != NULL || strstr(bytes[2], "7") != NULL) &&
      (strstr(bytes[3], "4") != NULL || strstr(bytes[3], "6") != NULL || strstr(bytes[3], "8") != NULL))
    printf("Es un ZIP\n");
  // PDF
  else if(strstr(bytes[0], "25") != NULL &&
      strstr(bytes[1], "50") != NULL &&
      strstr(bytes[2], "44") != NULL &&
      strstr(bytes[3], "46") != NULL &&
      strstr(bytes[4], "2D") != NULL)
    printf("Es un PDF\n");
  // MP3
  else if(
      (strstr(bytes[0], "FF") != NULL &&
      strstr(bytes[1], "FB") != NULL)
      ||
      (strstr(bytes[0], "49") != NULL &&
      strstr(bytes[1], "44") != NULL &&
      strstr(bytes[2], "33") != NULL))
    printf("Es un MP3\n");
  // EXE
  else if(strstr(bytes[0], "4D") != NULL &&
      strstr(bytes[1], "5A") != NULL)
    printf("Es un EXE\n");
  else
    printf("Archivo no identificado\n");

  // Liberamos memoria

  for(int i = 0; i < 8; i++){
    free(bytes[i]);
  }

  free(bytes);

  fclose(f);
}
