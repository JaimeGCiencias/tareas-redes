#include <openssl/evp.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void hash_md5(unsigned char*, long, char*);
void hash_sha1(unsigned char*, long, char*);
void hash_sha256(unsigned char*, long, char*);

// https://stackoverflow.com/questions/10521061/how-to-get-an-md5-checksum-in-powershell
int main(int argc, char **argv)
{
  if(argc < 3)
  {
    fprintf(stderr, "# de argumentos incorrecto\n");
    exit(1);
  }

  for(int i = 2; i < argc; i++)
  {
    FILE *fp;
    long long_archivo;
    unsigned char *buf;

    fp = fopen(argv[i], "rb");
    fseek(fp, 0L, SEEK_END);
    long_archivo = ftell(fp);
    fseek(fp, 0L, SEEK_SET);
    buf = malloc(long_archivo);
    fread(buf, long_archivo, 1, fp);
    fclose(fp);

    if(strcmp(argv[1],"md5") == 0) { hash_md5(buf, long_archivo, argv[i]); }
    else if(strcmp(argv[1],"sha1") == 0) { hash_sha1(buf, long_archivo, argv[i]); }
    else if(strcmp(argv[1],"sha256") == 0) { hash_sha256(buf, long_archivo, argv[i]); }
    else {
      free(buf);
      fprintf(stderr, "Cifrado no soportado\n");
      exit(1);
    }
    free(buf);
  }

  return 0;
}

void hash_md5(unsigned char* buf, long long_archivo, char* archivo)
{
  unsigned char *md5_result = NULL;
  md5_result = malloc(MD5_DIGEST_LENGTH);
  MD5(buf, long_archivo, md5_result);
  for (int i=0; i < MD5_DIGEST_LENGTH; i++)
  {
    printf("%02x",  md5_result[i]);
  }
  printf("  %s\n", archivo);
  free(md5_result);
}

void hash_sha1(unsigned char* buf, long long_archivo, char* archivo)
{
  unsigned char *sha1_result = NULL;
  sha1_result = malloc(SHA_DIGEST_LENGTH);
  SHA1(buf, long_archivo, sha1_result);
  for (int i=0; i < SHA_DIGEST_LENGTH; i++)
  {
    printf("%02x",  sha1_result[i]);
  }
  printf("  %s\n", archivo);
  free(sha1_result);
}

void hash_sha256(unsigned char* buf, long long_archivo, char* archivo)
{
  unsigned char *sha256_result = NULL;
  sha256_result = malloc(SHA256_DIGEST_LENGTH);
  SHA256(buf, long_archivo, sha256_result);
  for (int i=0; i < SHA256_DIGEST_LENGTH; i++)
  {
    printf("%02x",  sha256_result[i]);
  }
  printf("  %s\n", archivo);
  free(sha256_result);
}
