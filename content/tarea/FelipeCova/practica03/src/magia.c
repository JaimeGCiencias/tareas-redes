#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int obtener_formato(unsigned char, unsigned char, unsigned char, unsigned char);
void imprimir_formato(char*, int);

#define GIF 1
#define ELF 2
#define PNG 3
#define ZIP 4
#define PDF 5
#define MP3 6
#define EXE 7

int main(int argc, char **argv)
{
  if(argc < 2)
  {
    fprintf(stderr, "Incluir al menos un archivo\n");
    exit(1);
  }

  int num_archivos = argc - 1;



  for(int i = 0; i < num_archivos; i++)
  {
    FILE *fp;
    unsigned char buffer[4];
    fp = fopen (argv[i+1], "rb");
    fread(buffer, sizeof(buffer), 1, fp);
    int formato = obtener_formato(buffer[0],buffer[1],buffer[2],buffer[3]);
    imprimir_formato(argv[i+1], formato);
    fclose(fp);
  }

  return 0;
}

int obtener_formato(unsigned char b1, unsigned char b2, unsigned char b3, unsigned char b4)
{
  if(b1 == 71 && b2 == 73 && b3 == 70 && b4 == 56) { return GIF; }
  if(b1 == 127 && b2 == 69 && b3 == 76 && b4 == 70) { return ELF; }
  if(b1 == 137 && b2 == 80 && b3 == 78 && b4 == 71) { return PNG; }
  if(b1 == 80 && b2 == 75 && b3 == 3 && b4 == 4) { return ZIP; }
  if(b1 == 37 && b2 == 80 && b3 == 68 && b4 == 70) { return PDF; }
  if(b1 == 73 && b2 == 68 && b3 == 51) { return MP3; }
  if(b1 == 77 && b2 ==  90) { return EXE; }
  return -1;
}

void imprimir_formato(char* archivo, int formato)
{
  char msj[100];
  char msj2[12] = "Archivo ";
  strcpy(msj, archivo);
  strcat(msj, ": ");
  strcat(msj, msj2);
  char *ext; 

  switch(formato)
  {
    case(GIF):
    ext = "GIF";
    break;

    case(ELF):
    ext = "ELF";
    break;

    case(PNG):
    ext = "PNG";
    break;

    case(ZIP):
    ext = "ZIP";
    break;

    case(PDF):
    ext = "PDF";
    break;

    case(MP3):
    ext = "MP3";
    break;

    case(EXE):
    ext = "EXE";
    break;

    default:
    ext = "no identificado";
  }

  strcat(msj, ext);

  printf("%s\n", msj);
}
