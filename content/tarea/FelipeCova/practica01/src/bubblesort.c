#include <stdio.h>
#include <stdlib.h>

void intercambia(int *arr[], int i, int j)
{
  int *temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp; 
}

void bubbleSort(int *arr[], int tam) 
{ 
  int n = tam;
  
  while(n != 0)
  {
    int intercambiados = 0;
    int i;
    for(i = 0; i < n - 1; i++)
    {
      if(*arr[i] > *arr[i + 1])
      {
        intercambia(arr, i, i + 1);
        intercambiados = 1;
      }
    }
    n--;
  }
}

int main(int argc, char **argv)
{
  int long_arreglo = argc - 1;
  int *apuntadores_num[long_arreglo];
  
  int i;
  for(i = 0; i < long_arreglo; i++)
  {
    int num = atoi(argv[i + 1]);
    int *ap_num = (int*) malloc(sizeof (ap_num));
    *ap_num = num;
    apuntadores_num[i] = ap_num;
  }
  
  printf("Arreglo: ");
  for(i = 0; i < long_arreglo; i++)
  {
    printf("%d ", *apuntadores_num[i]);
  }
  puts("");
  
  bubbleSort(apuntadores_num, long_arreglo);
  
  printf("Ordenado: ");
  for(i = 0; i < long_arreglo; i++)
  {
    printf("%d ", *apuntadores_num[i]);
  }
  
  puts("");
    
  return 0;
}
