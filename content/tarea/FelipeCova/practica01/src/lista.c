#include <stdio.h>
#include <stdlib.h>
#include "lista.h"

void imprime(void *elemento)
{
  char *e = (char*)elemento;
  printf("%c ", *e);
}

void mayuscula(void *elemento)
{
  char *e = (char*)elemento;
  *e += 'A' - 'a';
}

// int main()
// {
//   struct lista lista = { cabeza: 0, longitud:0 };
//   for (int c = 'a'; c <= 'k'; c++)
//   {
//     char *e = (char *) malloc(sizeof (e));
//     *e = c;
//     agrega_elemento (&lista, e);
//   }
//
//   printf ("Longitud de lista: %d\nElementos: ", lista.longitud);
//
//   aplica_funcion (&lista, imprime);
//   aplica_funcion (&lista, mayuscula);
//   printf ("\nMayusculas: ");
//   aplica_funcion (&lista, imprime);
//
//   int n = lista.longitud;
//
//   for (int i = 0; i < n; i++)
//   {
//     printf("\nElemento a ser eliminado: %c, ", *(char*)obten_elemento(&lista, 0));
//     char *e = (char*)elimina_elemento(&lista, 0);
//     printf ("Elemento eliminado: %c, longitud de lista: %d", *e, lista.longitud);
//     free(e);
//   }
//   puts ("");
//   return 0;
// }


void agrega_elemento(struct lista *lista, void *elemento)
{
  struct nodo *nuevo = (struct nodo*)malloc(sizeof(struct nodo));
  nuevo->elemento = elemento;
  nuevo->siguiente = NULL;

  if(lista->longitud == 0)
  {
    lista->cabeza = nuevo;
    lista->longitud = 1;
    return;
  }

  struct nodo *nodo_temp = lista->cabeza;

  while(nodo_temp->siguiente != NULL) { nodo_temp = nodo_temp->siguiente; }

  nodo_temp->siguiente = nuevo;
  lista->longitud++;
}

void* obten_elemento(struct lista* lista, int n)
{

  if(n < 0 || n >= lista->longitud) { return 0; }

  struct nodo *nodo_temp = lista->cabeza;

  int i;
  for(i = 0; i < n; i++)
  {
    if(i == n) { break; }
    nodo_temp = nodo_temp->siguiente;
  }
  return nodo_temp->elemento;
}

void* elimina_elemento(struct lista * lista, int n)
{

  if(n < 0 || n >= lista->longitud) { return 0; }

  void* elemento = (void*)malloc(sizeof(elemento));

  // Lista de un solo elemento
  if(lista->longitud == 1) {
    struct nodo *borrado = lista->cabeza;
    elemento = borrado->elemento;
    lista->cabeza = NULL;
    lista->longitud = 0;
    free(borrado);
    return elemento;
  }

  // Eliminar cabeza de la lista
  if(n == 0)
  {
    struct nodo *borrado = lista->cabeza;
    elemento = borrado->elemento;
    lista->cabeza = lista->cabeza->siguiente;
    lista->longitud--;
    free(borrado);
    return elemento;
  }

  struct nodo *nodo_temp = lista->cabeza;

  int i;
  for(i = 0; i < n; i++)
  {
    if(i == n - 1) { break; }
    nodo_temp = nodo_temp->siguiente;
  }

  struct nodo *borrado = nodo_temp->siguiente;
  elemento = borrado->elemento;

  nodo_temp->siguiente = nodo_temp->siguiente->siguiente;
  lista->longitud--;
  free(borrado);
  return elemento;
}

void aplica_funcion(struct lista *lista, void (*f)(void *))
{
  struct nodo *nodo_temp = lista->cabeza;
  while(nodo_temp != NULL)
  {
    f(nodo_temp->elemento);
    nodo_temp = nodo_temp->siguiente;
  }
}
