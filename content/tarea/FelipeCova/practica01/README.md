## Cova Pacheco Felipe de Jesús
##### Compilar:
make
##### Eliminar ejecutables:
make clean
##### Ejecutar:
1. Archivos: ./archivos
2. BubbleSort: ./bubblesort < lista de enteros >
3. Password: ./password < archivo de contraseñas >

##### Compilar
![Imagen 1](screenshots/im1.png)
### Archivos
![Imagen 2](screenshots/im2.png)
![Imagen 3](screenshots/im3.png)
![Imagen 4](screenshots/im4.png)
![Imagen 5](screenshots/im5.png)
![Imagen 6](screenshots/im6.png)
![Imagen 7](screenshots/im7.png)
### BubbleSort
![Imagen 8](screenshots/im8.png)
### Password
![Imagen 9](screenshots/im9.png)
