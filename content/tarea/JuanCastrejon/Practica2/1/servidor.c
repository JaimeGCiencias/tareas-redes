#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
  int listenfd = 0, connfd = 0, valread;
  struct sockaddr_in serv_addr;

  FILE *stream;
  char recvBuff[1024];
  char cmdBuff[1024];

  if (argc != 2)
  {
    printf("Uso: %s <puerto>\n", argv[0]);
    return 1;
  }

  listenfd = socket(AF_INET, SOCK_STREAM, 0);
  if (listenfd == -1)
  {
    printf("Socket creation failed.\n");
    exit(0);
  }
  else
    printf("Socket successfully created.\n");
  memset(&serv_addr, '0', sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  serv_addr.sin_port = htons(atoi(argv[1]));

  if (bind(listenfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) != 0)
  {
    printf("Socket bind failed.\n");
    exit(0);
  }
  else
    printf("Socket successfully binded.\n");

  if ((listen(listenfd, 5)) != 0)
  {
    printf("Listen failed...\n");
    exit(0);
  }
  else
    printf("Server listening.\n");
  connfd = accept(listenfd, (struct sockaddr *)NULL, NULL);
  if (connfd < 0)
  {
    printf("Server acccept failed.\n");
    exit(0);
  }
  else
    printf("Server acccepted the client.\n");
  while (1)
  {
    bzero(recvBuff, sizeof(recvBuff));
    bzero(cmdBuff, sizeof(cmdBuff));
    read(connfd, recvBuff, sizeof(recvBuff));
    if (strncmp(recvBuff, "EOF", 3) == 0)
    {
      write(connfd, "EOF", 3);
      break;
    }
    strcat(recvBuff, " 2>&1");
    stream = popen(recvBuff, "r");
    char ch;
    int n = 0;
    while ((ch = fgetc(stream)) != EOF)
      cmdBuff[n++] = ch;
    fgets(cmdBuff, sizeof(cmdBuff), stream);
    write(connfd, cmdBuff, sizeof(cmdBuff));
  }
  close(connfd);
}