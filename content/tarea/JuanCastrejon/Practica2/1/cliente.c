#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

int main(int argc, char *argv[])
{
  int sockfd = 0, n = 0;
  char recvBuff[1024];
  struct sockaddr_in serv_addr;

  if (argc != 3)
  {
    printf("Uso: %s <ip del servidor> <puerto>\n", argv[0]);
    return 1;
  }

  memset(recvBuff, '0', sizeof(recvBuff));
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf("Error: No se pudo crear el socket.\n");
    return 1;
  }

  memset(&serv_addr, '0', sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(atoi(argv[2]));

  if (inet_pton(AF_INET, argv[1], &serv_addr.sin_addr) <= 0)
  {
    printf("inet_pton error\n");
    return 1;
  }

  if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    printf("Error de conexión.\n");
    return 1;
  }

  char command[1024];
  printf("Para terminar conexión escribe 'EOF'\n");
  while (1)
  {
    bzero(command, sizeof(command));
    bzero(recvBuff, sizeof(recvBuff));
    fgets(command, sizeof(command), stdin);
    command[strlen(command)-1]='\0';
    write(sockfd, command, strlen(command));
    read(sockfd, recvBuff, sizeof(recvBuff));
    if (strncmp(recvBuff, "EOF", 3) == 0)
      break;
    printf("%s", recvBuff);
  }
  return 0;
}