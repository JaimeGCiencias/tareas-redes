# Práctica 2: Sockets

### Ejercicio: Bind shell

Ejemplo de ejecución:
![Ejemplo 1](imgs/1.png)

## Conclusiones
Se utilizaron sockets y la función popen() para crear un servidor que permite la conexión de un cliente y procesa los comandos especificados por este último. Se redirigió la salida de errores a la salida estándar para mostrar los errores de comandos al cliente.