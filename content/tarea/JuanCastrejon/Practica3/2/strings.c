#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
  if (argc != 2)
  {
    printf("Uso: %s <archivo>\n", argv[0]);
    return 1;
  }

  FILE *fp;
  fp = fopen(argv[1], "rb");
  int lineBreak = 0;
  if (fp != NULL)
  {
    char ch;
    while((ch = fgetc(fp)) != EOF) {
      if(32 <= ch && ch <= 126) {
        printf("%c", ch);
        lineBreak = 1;
      }
      else {
        if(lineBreak) {
          printf("\n");
          lineBreak = 0;
        }
      }
    }
    printf("\n");
    fclose(fp);
  }
  else
  {
    perror(argv[1]);
  }
}
