1. tcpdump ether host 60:a4:4c:89:3c:a4 or ether dst fc:fb:fb:01:fa:21
2. tcpdump "ether [0:4] & 0xffffff00 == 0x88539500 or ether [6:4] & 0xffffff00 == 0x88539500"
3. tcpdump ether proto 0x86DD
4. tcpdump ether proto 0x0806 and ether dst ff:ff:ff:ff:ff:ff
5. tcpdump net 172.0.0.0/8
