#include <stdio.h>
#include <string.h>
#include <openssl/md5.h>
#include <openssl/sha.h>

int main(int argc, char **argv)
{
  if (argc < 2)
  {
    printf("Uso: %s <archivo1> [<archivo2>, ...]\n", argv[0]);
    return 1;
  }

  FILE *fp;
  int i = 1;
  while (++i < argc)
  {
    fp = fopen(argv[i], "rb");
    if (fp != NULL)
    {
      int bytes;
      unsigned char data[1024];
      if (strcmp(argv[1], "md5") == 0)
      {
        unsigned char c[MD5_DIGEST_LENGTH];
        MD5_CTX mdContext;
        MD5_Init(&mdContext);
        while ((bytes = fread(data, 1, 1024, fp)) != 0)
          MD5_Update(&mdContext, data, bytes);
        MD5_Final(c, &mdContext);
        for (int j = 0; j < MD5_DIGEST_LENGTH; j++)
          printf("%02x", c[j]);
        printf(" %s\n", argv[i]);
      }
      if (strcmp(argv[1], "sha1") == 0)
      {
        unsigned char c[SHA_DIGEST_LENGTH];
        SHA_CTX shaContext;
        SHA1_Init(&shaContext);
        while ((bytes = fread(data, 1, 1024, fp)) != 0)
          SHA1_Update(&shaContext, data, bytes);
        SHA1_Final(c, &shaContext);
        for (int j = 0; j < SHA_DIGEST_LENGTH; j++)
          printf("%02x", c[j]);
        printf(" %s\n", argv[i]);
      }
      if (strcmp(argv[1], "sha256") == 0)
      {
        unsigned char c[SHA256_DIGEST_LENGTH];
        SHA256_CTX shaContext;
        SHA256_Init(&shaContext);
        while ((bytes = fread(data, 1, 1024, fp)) != 0)
          SHA256_Update(&shaContext, data, bytes);
        SHA256_Final(c, &shaContext);
        for (int j = 0; j < SHA256_DIGEST_LENGTH; j++)
          printf("%02x", c[j]);
        printf(" %s\n", argv[i]);
      }
      fclose(fp);
    }
    else
    {
      perror(argv[i]);
    }
  }
  return 0;
}
