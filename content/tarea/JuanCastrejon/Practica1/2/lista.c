#include <stdio.h>
#include <stdlib.h>
#include "lista.h"

void agrega_elemento(struct lista *lista, void *elemento)
{
  if (lista->cabeza == NULL)
  {
    struct nodo *n = malloc(sizeof(struct nodo));
    n->elemento = elemento;
    n->siguiente = NULL;
    lista->cabeza = n;
  }
  else
  {
    struct nodo *n = lista->cabeza;
    while (n->siguiente != NULL)
    {
      n = n->siguiente;
    }
    struct nodo *m = malloc(sizeof(struct nodo));
    m->elemento = elemento;
    m->siguiente = NULL;
    n->siguiente = m;
  }
  lista->longitud++;
}

void *obten_elemento(struct lista *lista, int n)
{
  if (lista->longitud < 0 || n >= lista->longitud)
  {
    return 0;
  }
  int i = 0;
  struct nodo *actual = lista->cabeza;
  while (i < n)
  {
    actual = actual->siguiente;
    i++;
  }
  return actual->elemento;
}

void *elimina_elemento(struct lista *lista, int n)
{
  if (lista->longitud < 0 || n >= lista->longitud)
  {
    return 0;
  }
  if (n == 0)
  {
    struct nodo *e = lista->cabeza;
    if(lista->longitud > 1) {
      lista->cabeza = e->siguiente;
    }
    void *elemento = e->elemento;
    free(e);
    lista->longitud--;
    return elemento;
  }
  else
  {
    int i = 0;
    struct nodo *actual = lista->cabeza;
    while (i < n - 1)
    {
      actual = actual->siguiente;
      i++;
    }
    struct nodo *e = actual->siguiente;
    void *elemento = e->elemento;
    if(n != lista->longitud - 1)
    {
      actual->siguiente = e->siguiente;
    }
    free(e);
    lista->longitud--;
    return elemento;
  }
}

void aplica_funcion(struct lista *lista, void (*f)(void *))
{
  struct nodo *n = lista->cabeza;
  while (n != NULL)
  {
    (*f)(n->elemento);
    n = n->siguiente;
  }
}
