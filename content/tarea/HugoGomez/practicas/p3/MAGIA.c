#include <stdio.h>
#include <stdlib.h>
#include<string.h>

void subStringMod(char entradaT[20], char nombreT[50]);

int main(int argc, char **argv){

  FILE *fd;
  int c ;
  char entrada[20];
  char nombre[50];
  int j = 0;
  while(j<argc){
    fd = fopen(argv[j], "rt");
    if(fd == NULL){
      printf("Error al leer el archivo\n");
      exit(1);
    }
    int i = 0;
    while(i<20){
      c=fgetc(fd);
      entrada[i] = c;
      i = i + 1;
    }
    strcpy(nombre, argv[j]);
    subStringMod(entrada, nombre);
    entrada[0] = '\0';
    j = j + 1;
    fclose(fd);
  }
}

void subStringMod(char entradaT[20], char nombreT[50]){

  int i = 0;
  int gif = 0;
  int elf = 0;
  int png = 0;
  int zip = 0;
  int pdf = 0;
  int mp3 = 0;
  int exe = 0;
  while(i<20){
    char temp = entradaT[i];
    if(temp == 'g' || temp == 'G'){
      gif = gif + 1;
      png = png + 1;
    }else if(temp == 'i' || temp == 'I'){
      gif = gif + 1;
      zip = zip + 1;
    }else if(temp == 'f' || temp == 'F'){
      gif = gif + 1;
      elf = elf + 1;
      pdf = pdf + 1;
    }else if(temp == 'e' || temp == 'E'){
      elf = elf + 1;
      exe = exe + 1;
    }else if(temp == 'l' || temp == 'L'){
      elf = elf + 1;
    }else if(temp == 'p' || temp == 'P'){
      png = png + 1;
      pdf = pdf + 1;
      zip = zip + 1;
      mp3 = mp3 + 1;
    }else if(temp == 'n' || temp == 'N'){
      png = png + 1;
    }else if(temp == 'z' || temp == 'Z'){
      zip = zip + 1;
    }else if(temp == 'd' || temp == 'D'){
      pdf = pdf + 1;
    }else if(temp == 'm' || temp == 'M'){
      mp3 = mp3 + 1;
    }else if(temp == '3'){
      mp3 = mp3 + 1;
    }else if(temp == 'x' || temp == 'X'){
      exe = exe + 1;
    }
    i = i + 1;
  }
  if(gif >= 3){
    printf("%s \t\t: Archivo GIF\n", nombreT);
  }else if(elf >= 3){
    printf("%s \t\t: Archivo ELF\n", nombreT);
  }else if(png >= 3){
    printf("%s \t\t: Archivo PNG\n", nombreT);
  }else if(zip >= 3){
    printf("%s \t\t: Archivo ZIP\n", nombreT);
  }else if(pdf >= 3){
    printf("%s \t\t: Archivo PDF\n", nombreT);
  }else if(mp3 >= 3){
    printf("%s \t\t: Archivo MP3\n", nombreT);
  }else if(exe >= 3){
    printf("%s \t\t: Archivo EXE\n", nombreT);
  }


}
