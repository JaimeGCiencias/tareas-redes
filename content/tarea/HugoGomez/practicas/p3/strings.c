#include <stdio.h>
#include <stdlib.h>
#include<string.h>

int main(int argc, char **argv){
  FILE *fd;
  int c ;
  fd = fopen(argv[1], "rt");
  if(fd == NULL){
    printf("Error al leer el archivo\n");
    exit(1);
  }
  int salto = 0;
  int estuvo = 0;
  while((c = fgetc(fd))!=EOF){

    if(c>=32 && c<=126){
      printf("%c", c);
      salto = 0;
      estuvo = 1;
    }else{
      salto = 1;
    }
    if(salto==1 && estuvo == 1){
      printf("\n");
      salto = 2;
      estuvo = 2;
    }
  }
}
