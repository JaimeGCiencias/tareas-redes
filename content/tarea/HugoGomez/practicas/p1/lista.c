#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include"lista.h"

int cero = 0;

void agrega_elemento(struct lista * lista, void * elemento){

    struct nodo * nuevo = malloc(2*sizeof(struct nodo));
    nuevo->elemento = elemento;
    nuevo->siguiente = NULL;
    int tam = lista->longitud;
    if(tam == 0){
        lista->cabeza = nuevo;
    }else{
        struct nodo *temporal = lista->cabeza;
        while(temporal->siguiente != NULL){
            temporal = temporal->siguiente;
        }
        temporal->siguiente = nuevo;
    }
    tam = tam + 1;
    lista->longitud = tam;
}

void * obten_elemento(struct lista * lista, int n){

    struct nodo *temporal = lista->cabeza;
    if(n<0 || n>=lista->longitud){
      return 0;
    }
    int i = 0;
    while(i < n){
        i = i + 1;
        temporal = temporal->siguiente;
    }
    return temporal->elemento;

}

void * elimina_elemento(struct lista * lista, int n){
    int *regreso = &cero;
    int tamLista = lista->longitud;
    if(tamLista <= n || n<0){
        return  regreso;
    }else{
      struct nodo *temporal = lista->cabeza;
      if(n == 0){//eliminamos solo el primer elemento
          if(tamLista == 1){
              void * elem = temporal->elemento;
              lista->cabeza = NULL;
              lista->longitud = tamLista - 1;
              return elem;
          }else{
              void * elem = temporal->elemento;
              temporal = temporal->siguiente;
              lista->cabeza = temporal;
              lista->longitud = tamLista - 1;
              return elem;
          }
      }else if(n == (tamLista-1)){//eliminamos el ultimo elemento
          int i = 1;
          while(i<tamLista-1){
            temporal = temporal->siguiente;
            i = i + 1;
          }
          void * elem = temporal->siguiente->elemento;
          temporal->siguiente = NULL;
          lista->longitud = tamLista - 1;
          return elem;
      }else{
        int i = 0;
        while(i < n-1){
          temporal = temporal->siguiente;
          i = i + 1;
        }
        void * elem = temporal->siguiente->elemento;
        temporal->siguiente = temporal->siguiente->siguiente;
        lista->longitud = tamLista - 1;
        return elem;
      }
    }
}

void aplica_funcion(struct lista * lista, void (*f)(void *)){

    struct nodo *temporal = lista->cabeza;
    while(temporal != NULL){
        f(temporal->elemento);
        temporal = temporal->siguiente;
    }
}

void imprime(void * elemento) {
    char * e = (char*)elemento;
    printf("%c ", *e);
}

void imprimeCadena(void * elemento) {
    char *cad2 = (char*)elemento;
    printf("%s \n", cad2);
}

void mayuscula(void * elemento) {
    char * e = (char*)elemento;
    *e += 'A' - 'a';
}

void imprime_todos_los_elementos(struct lista * lista){

    struct nodo *temporal = lista->cabeza;
    while(temporal != NULL){
        char cad1[30];
        strcpy(cad1, temporal->elemento);
        //char *a = (char*)temporal->elemento;
        printf("elemento: %s\n", cad1);
        temporal = temporal->siguiente;
    }
}

/*
int main() {
  struct lista lista = {cabeza:0, longitud:0};

  for(int c = 'a'; c <= 'b'; c++) {
    char * e = (char*)malloc(sizeof(e));
    *e = c;
    agrega_elemento(&lista, e);
  }
  printf("Longitud de lista: %d\nElementos: ", lista.longitud);
  aplica_funcion(&lista, imprime);
  aplica_funcion(&lista, mayuscula);
  printf("\nMasyúsculas: ");
  aplica_funcion(&lista, imprime);
  int n = lista.longitud;
  for(int i = 0; i < n; i++) {
    printf("\nElemento a ser eliminado: %c, ", *(char*)obten_elemento(&lista, 0));
    char * e = (char *)elimina_elemento(&lista, 0);
    printf("elemento eliminado: %c, longitud de lista: %d", *e, lista.longitud);
    free(e);
  }
  puts("");
}
*/
