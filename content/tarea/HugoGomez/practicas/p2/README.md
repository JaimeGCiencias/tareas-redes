# Universidad Nacional Autónoma de México
## Facultad de ciencias
### Redes de computadoras
### Practica 2

#### Compilar
Para compilar y ejecutar el archivo "servidor.c" solo es necesario abrir la terminal y colocarse dentro del directorio donde se encuentra el código fuente y ejecutar el comando "make servidor". Ese comando compila y ejecuta el archivo "servidor.c".

Para compilar y ejecutar el archivo "cliente.c" solo es necesario abrir al terminal y colocarse dentro del directorio donde sen encuentra el código fuente y ejecutar el comando "make cliente". Este comando compila y ejecuta el archivo "cliente.c".

![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p2/imagenes/comServ.png)

![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p2/imagenes/comClie.png)
Notas:
- Primero se debe de ejecutar el comando "make servidor".
- El puerto por defecto es 5555 y la ip es 127.0.0.1 .


####  Mensaje cliente y respuesta.
Para mandar mensaje desde cliente es necesario que el cliente se conecte con el servidor. Una vez conectado se le solicita al cliente que escriba un mensaje, una vez escrito se le envía al servidor y este le responde dependiendo del mensaje que se le envía.
Si se envía un mensaje que la terminal pueda reconocer se responde con la respuesta de la terminal.

![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p2/imagenes/clieComaB.png)

![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p2/imagenes/servComaB.png)

Si se envía la palabra "EOF" el servidor termina la ejecución.

![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p2/imagenes/clieEOF.png)

![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p2/imagenes/servEOF.png)

Si se envía un mensaje que la terminal no pueda reconocer se responde con el error que muestra la terminal.

![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p2/imagenes/clieComaM.png)

![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p2/imagenes/servComaM.png)
