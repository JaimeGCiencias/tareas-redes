#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>
#include <sys/types.h>
#include <netdb.h>
# include<unistd.h>


void error ( char * mensaje ) {
  fprintf ( stderr , " %s \n" , mensaje );
  exit (1);
}
int main ( int argc , char * argv []) {
  if ( argc < 3) {
    fprintf ( stderr , " Uso : %s < direccion IP > < puerto >\n", argv [0]);
    return 1;
  }
  int sockfd;
  struct sockaddr_in host_addr;
  if (( sockfd = socket ( AF_INET , SOCK_STREAM , 0)) < 0){
    error ( " Error al crear socket \n " );
  }
  int opt = 1;
  if ( setsockopt ( sockfd , SOL_SOCKET , SO_REUSEADDR , &opt , sizeof ( int )) < 0){
    error ( " Error en setsockopt \n " );
  }
  memset (&host_addr , 0 , sizeof( host_addr ));
  host_addr.sin_family = AF_INET ;
  host_addr.sin_port = htons ( atoi ( argv [2]));
  if ( inet_pton ( AF_INET , argv[1] , &host_addr.sin_addr ) <= 0){
    error ( " Error al leer direccion IP de servidor \n" );
  }
  if ( connect ( sockfd , ( struct sockaddr *)&host_addr, sizeof ( host_addr )) < 0){
    error ( " Error al conectarse a servidor \n" );
  }
  char msg [256];
  memset(msg, 0, 256);
  printf( "Ingresa mensaje a enviar:\n" );
  fgets(msg, 256, stdin);
  //scanf("%s", msg);
  send(sockfd, msg, strlen(msg), 0);

  //---------------EMPIEZA ESCUCHA DE RESPUESTA---------------------------------
  int fd, numbytes;
  struct sockaddr_in server;
  struct hostent *he;
  char buf[256];
  char *ip;
  ip=argv[1];
  if ((he=gethostbyname(ip))==NULL){
   /* llamada a gethostbyname() */
    printf("gethostbyname() error\n");
    exit(-1);
  }
  //Paso 2, definicion de socket
  if ((fd=socket(AF_INET, SOCK_STREAM, 0))==-1){
     /* llamada a socket() */
    printf("socket() error\n");
    exit(-1);
  }
  server.sin_family = AF_INET;
  server.sin_port = htons ( atoi ( argv [2]));
  server.sin_addr = *((struct in_addr *)he->h_addr);
  bzero(&(server.sin_zero),8);
  //Paso 3, conectarnos al servidor
  if(connect(fd, (struct sockaddr *)&server,
  sizeof(struct sockaddr))==-1){
  /* llamada a connect() */
  printf("connect() error\n");
  exit(-1);
  }

 if ((numbytes=recv(fd,buf,100,0)) == -1){
  /* llamada a recv() */
  printf("Error en recv() \n");
  exit(-1);
  }

 buf[numbytes]='\0';

 printf("Mensaje del Servidor: %s\n",buf);
  /* muestra el mensaje de bienvenida del servidor =) */

 close(fd);

  return 0;
}
