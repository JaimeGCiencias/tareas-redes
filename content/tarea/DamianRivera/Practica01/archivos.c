#include"lista.c"
#include "auxiliares.c"
#include<stdio.h>
#include<stdlib.h>

//Función principal para la ejecución del programa.
int main(){
    //Lista donde se agregarán los elementos durante el programa.
    Lista lista = {cabeza:0, longitud: 0};

    int opcion, indice;

    //Ciclo infinito hasta seleccionar la opción 5.
    while(1){
        //Impresión del menú. 
        printf("\tElegir opción:\n\n");
        printf(" 1. Insertar archivo.\n 2. Leer archivo.\n 3. Eliminar archivo.\n 4. Imprimir archivos.\n 5. Salir.");
        printf("\n\n  Índice opción: ");    

        //Memoria donde se guardarán el nombre de los archivos ingresados.
        char * elemento = malloc(sizeof(char));
        scanf("%d", &opcion);//escaner para ingresar una opción mostrada en el menú.

        switch (opcion)
        {
            //Caso en el que se ingresará un archivo.
            case 1:
                printf("Nombre del elemento a insertar: ");
                scanf("%s", elemento);//Se guarda la cadena del nombre del archivo.
                agrega_elemento(&lista, elemento);
                puts("\n");
                break;
            //Caso en el que se leerá un archivo.
            case 2:

                printf("Índice del elemento a leer: ");
                char * archivo = malloc(sizeof(char));//Nombre del alemento.
                scanf("%d", &indice);//Se guarda el índice dentro de la lista.
                archivo = obten_elemento(&lista, indice);//Obtenemos el elemento de la lista

                DIR * folfer;
                folfer = opendir(archivo);
                //Verificamos si no es un directorio
                if (folfer == NULL) 
                {
                    leerArchivo(archivo);//Cargamos un archivo.
                }
                //Si es un directorio lo leemos.
                else
                {
                    leerDirectorio(archivo);//Cargamos un directorio. 
                }
                closedir(folfer);
                puts("\n");
                break;
            //Caso para eliminar un archivo.
            case 3:
                printf("Índice del elemento a eliminar: ");
                scanf("%d", &indice);//Se guarda el índice dentro de la lista.
                elimina_elemento(&lista, indice);
                puts("\n");
                break;
            //Caso para imprimir los elementos.
            case 4:
                printf("Los elementos son: ");
                // Se llama la función que aplica la función que imprime cadenas del 
                // nombre de los elementos en la lista.
                aplica_funcion(&lista, imprime);
                printf("\n\n");
                break;
            //Caso para terminar el programa
            case 5:
                //liberamos la memoria utilizada.
                if (lista.longitud) {
                    aplica_funcion(&lista, free);
                    memset(&lista, 0, sizeof(Lista));
                    
                }
                return 0;
            default:
                printf("No se reconoce la opción.\n");
                break;
        }
    }
    return 0;
}