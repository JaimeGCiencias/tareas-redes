#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    FILE *file;//apuntador para el archivo /etc/passwd
    char* token;//token para imprimir dato por dato
    char puntos[2] = ":";//caracter para seperar los token
    char str[128];//tamaño de línea

    file = fopen("/etc/passwd", "r");
    if (file) {
        int i;
        //Leemos todas las líneas del archivo
        while (fgets (str, 128, file) != NULL)
        {
            i = 0;//índice para saber que token se imprimirá.
            token = strtok(str, puntos);

            //recorremos todos los tokens de la línea.
            while( token != NULL ) {
                switch (i)
                {
                    case 0:
                        printf("Nombre de Usuario: %s\n", token);
                        break;
                    case 1:
                        printf("Contraseña: %s\n", token);
                        break;
                    case 2:
                        printf("UID: %s\n", token);
                        break;
                    case 3:
                        printf("GID: %s\n", token);
                        break;
                    case 4:
                        printf("GECOS: %s\n", token);
                        break;
                    case 5:
                        printf("Home: %s\n", token);
                        break;
                    case 6:
                        printf("Shell: %s\n", token);
                        break;
                    default:
                        break;
                }
                i++;
                token = strtok(NULL, puntos);//continuamos seleccionando los tokens
            }
            puts("\n");
        }
        fclose(file);//cerramos el flujo del archivo.
    }
    return 0;
}
