##Práctica 5
- Rivera González Damian
- Vargas Aldaco Alejandro

### Inyección de Tramas

### DNS
Creamos una subdominio `.cf` en [`www.freenom.com`](https://www.freenom.com/en/index.html)

![](media/01dns.png)

Luego creamos una cuenta en [`dns.he.net`](https://dns.he.net/), vamos a la sección `Add a new domain`

![](media/09dns.png)

Cuando entramos a la página de administración de la zona nos muestra un error.

![](media/10dns.png)

Vamos a la página de administración del subdomino que registramos en `freenom.com` e ingresamos  a `Management Tools -> 
Nameservers`, seleccionar `Use custom nameservers` e ingresamos los servidores DNS de `dns.he.net`:

![](media/12dns.png)

Una vez que se haya activado la zona creada. Se quitará el error de `Delegacion`.

![](media/14dns.png)

Agregamos un nuevo registro de tipo A.

![](media/15dns.png)

![](media/16dns.png)

Y comprobamos con `nslookup` que se puede hacer la resolución de nombre del host a la dirección IP registrada:

![](media/17dns.png)


#### Preguntas
+ ¿Qué es un servidor DNS? 

Es un dispositivo conectado a Redes IP que hace traducciones entre nombres de dominio a direcciones IP, con base en su base 
de datos y la configuración que este tenga para logras resolver esas traducciones.

+ ¿Para qué se utiliza cada uno de los registros SOA, NS, A, AAAA, PTR, CNAME, MX y TXT?

SOA: proporciona información sobre el servidor DNS primario de la zona. 

NS: Define la asociación que existe entre un nombre de dominio y los servidores de nombres que almacenan la información de 
dicho dominio.

A: se usa para traducir nombres de servidores de alojamiento a direcciones IPv4.

AAAA: se usa para traducir nombres de host a direcciones IPv6.

PTR: también se conoce como "registro inverso", tiene la función inversa del registro A, traduciendo de una dirección IP a un 
nombre de dominio.

CNAME: (canonical name), crea nombres de servidores de alojamiento adicionales para los servidores de alojamiento de un dominio.

MX: (mail exchange) se asocia un nombre de dominio a una losta de servidores de intercambio de correo para ese dominio.

TXT: contiene información para fuentes externas al dominio.


+ ¿Qué hacen las herramientas `nslookup` y `dig`? 

`nslookup`: se utiliza para saber si el DNS está resolviendo correctamente 
los nombres y las IPs, con este se puede obtener la dirección IP desde el nombre de dominio o viceversa.

`dig`: (domain information groper) permite realizar consultas a los distintos registros DNS.


### Packet Tracer
#### Creación de redes.
#### Red 192.168.1.0/24

Creamos la primera red.
Un switch y dos computadoras.

![](media/02pt.png)

Configuramos las IP de las dos computadoras.

![](media/03pt.png)

![](media/04pt.png)

Verificamos que ambas computadoras pueden comunicarse.

![](media/05pt.png)

#### Red 10.10.10.0/24

Conectamos una PC, una impresora y un servidor a un switch.

![](media/06pt.png)

Asignamos una dirección IP estática al Servidor DHCP

![](media/07pt.png)

Configuramos el servidor DHCP en la pestaña `Services`, desactivando las opciones HTTP y HTTPS

![](media/08pt.png)

Vamos a la seccion DHCP, habilitamor el servicio DHCP, asignamos un gateway, un servidor DNS, una dirección IP inicial y una 
máscara de Red, y guardamos la configuración.

![](media/09pt.png)

Indicamos que se le asigne una dirección IP de manera dinámica a la computadora (PC2) conectada al servidor, a través de DHCP).

![](media/10pt.png)

De igual manera con la impresora.

![](media/11pt.png)

Verificamos que la computadora (PC2) se pueda comunicar con la impresora (printer0).

![](media/12pt.png)


#### Red 189.240.70.0/24

Conectamos dos servidores a un switch.

![](media/13pt.png)

Asignamos una dirección IP a. servidor web `www.redes.net`

![](media/14pt.png)

Asignamos una dirección IP al servidor DNS.

![](media/15pt.png)

Verificamos que se puedan comunicar entre ambos servidores.

![](media/16pt.png)

Habilitamos el servico de HTTP y HTTPS en `www.redes.net`

![](media/17pt.png)

Habilitamos el servicio DNS en el servidor DNS y creamos un registro de tipo A con los datos del servidor web:

![](media/18pt.png)


#### Conexión de Redes.

Conectamos las redes `192.168.1.0/24` y `10.10.10.0/24` con la red `189.240.70.0/24` a través de routers. Creamos tres 
routers y los conectamos entre ellas.

![](media/19pt.png)

Configuramos los routers de la siguiente manera.
Apagar router haciendo click en el botón de encendido

Agregamos una tarjeta de red de tipo `HWIC-2T`, arrastrándola a uno de los espacios vacios en el router, esta tarjeta permite 
transmitir datos a través de una `WAN` por medio de puertos seriales. Y luego de ello prendemos el router.

![](media/20pt.png)

Ingresar a la pestaña `CLI` para ingresar a la línea de comandos, entrar a modo privilegiado:

```
Router>enable
Router#
```

Entrar a la configuración global

```
Router#configure terminal
```

Cambiar _hostname_ de router

```
Router(config)#hostname Router0
Router0(config)#
```

Asignar una contraseña para entrar a modo privilegiado

```
Router0(config)#enable secret <contraseña>
```

Cada vez que se quiera entrar a modo privilegiado con `enable` se pedirá esta contraseña. Para guardar configuración, salir de configuración global y copiar la configuración actual a la configuración de inicialización:

```
Router0(config)#exit
Router0#copy running-config startup-config 
```

Conectar los routers con cables de tipo `Serial DTE` por medio de los puertos seriales de la siguiente manera:

![](media/21pt.png)

Asignar direcciones IP a interfaces de routers y prenderlas, esto puede hacerse por medio de interfaz gráfica o línea de comandos, se mostrará la configuración de Router0 por línea de comandos:

```
Router0>enable
Password: 
Router0#configure terminal
Enter configuration commands, one per line.  End with CNTL/Z.
Router0(config)#interface FastEthernet0/0
Router0(config-if)#ip address 189.240.70.1 255.255.255.0
Router0(config-if)#no shutdown

Router0(config-if)#
%LINK-5-CHANGED: Interface FastEthernet0/0, changed state to up

%LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0, changed state to up

Router0(config-if)#exit
Router0(config)#interface serial0/1/0
Router0(config-if)#ip address 189.240.220.1 255.255.255.252
Router0(config-if)#no shutdown

%LINK-5-CHANGED: Interface Serial0/1/1, changed state to down
Router0(config-if)#exit
Router0(config)#interface serial0/1/1
Router0(config-if)#ip address 189.240.230.1 255.255.255.252
Router0(config-if)#no shutdown
Router0(config-if)#exit
Router0(config)#exit
Router0#
%SYS-5-CONFIG_I: Configured from console by console

Router0#copy running-config startup-config
Destination filename [startup-config]? 
Building configuration...
[OK]
Router0#
```

Se utiliza el comando `interface <interfaz>` para acceder a la configuración de una interfaz, el comando `ip address <ip> <mascara>` para asignar una dirección IP a la interfaz y el comando `no shutdown` para prender la interfaz. 

Verificar configuración de router:

`Router0#show running-config`.

Crear rutas estáticas en Router1 y Router2 para comunicar las redes. Configurar Router1 de la siguiente manera:

```
Router1>enable
Password: 
Router1#configure terminal
Router1(config)#ip route 189.240.70.0 255.255.255.0 serial0/1/0
```
 
 El comando `ip route 189.240.70.0 255.255.255.0 serial0/1/0` indica que se envíe el tráfico dirigido a la red 189.240.70.0/24 por la interfaz serial0/1/0.
 
Repetimos para Router2.

#### Configuración de NAT/PAT 

Configurar PAT en Router1:

```
Router1#configure terminal
Router1(config)#access-list 1 permit 192.168.1.0 0.0.0.255
Router1(config)#ip nat inside source list 1 int serial0/1/0 overload
Router1(config)#interface serial0/1/0
Router1(config-if)#ip nat outside
Router1(config-if)#exit
Router1(config)#interface fa0/0
Router1(config-if)#ip nat inside
Router1(config-if)#exit
Router1(config)#exit
Router1#copy running-config startup-config
```

El comando `access-list 1 permit 192.168.1.0 0.0.0.255` crear una lista de acceso con la cual se permite el tráfico de la red 192.168.1.0/24. 

`ip nat inside source list 1 int serial0/1/0 overload` indica que las direcciones IP del tráfico permitido por la lista de acceso creada (la que tiene el identificador 1) sean traducidas por medio de PAT a la dirección IP asignada a la interfaz serial0/1/0.

`ip nat outside` indica que la interfaz se conecta al tráfico externo de NAT.

`ip nat inside` indica que la interfaz se conecta al tráfico interno de NAT.

Repetir lo anterior para Router2, cambiando la red de la lista de acceso.

Verificar que desde el navegador web de PC0 se pueda acceder a `www.redes.net`:

![](media/22pt.png)

Verificamos lo mismo para PC2:

![](media/23pt.png)

Para ver las estadísticas de NAT, usar el siguiente comando:

```
Router>show ip nat statistics
Total translations: 4 (0 static, 4 dynamic, 4 extended)
Outside Interfaces: Serial0/1/0
Inside Interfaces: FastEthernet0/0
Hits: 54  Misses: 8
Expired translations: 4
Dynamic mappings:
```
También se puede observar la tabla de traducciones:

```
Router>show ip nat translations
Pro  Inside global     Inside local       Outside local      Outside global
tcp 189.240.220.2:1025 192.168.1.2:1025   189.240.70.2:80    189.240.70.2:80
tcp 189.240.220.2:1026 192.168.1.2:1026   189.240.70.2:80    189.240.70.2:80
tcp 189.240.220.2:1027 192.168.1.2:1027   189.240.70.2:80    189.240.70.2:80
tcp 189.240.220.2:1028 192.168.1.2:1028   189.240.70.2:80    189.240.70.2:80
```

#### Preguntas
+ ¿Qué diferencia hay entre un switch y un router?
El switch envía los paquetes dentro de la red LAN, opera en la capa de enlace.
Los routers son capaces de escoger la mejor ruta actual para enviar paquetes de un origen al destino.
Los routers pueden interconectar varias redes.

+ ¿Qué tipos de switches hay?
LAN Switch
Unmanaged Network Switches
Managed Switches
PoE Switch
Stackable Switch

+ ¿Qué es una lista de acceso y para qué se utilizan? 

Permiten controlar el flujo del tráfico en equipos de redes, tales como enrutadores y conmutadores. Su principal objetivo es 
filtrar tráfico, permitiendo o denegando el tráfico de red de acuerdo a alguna condición

+ Se creó una lista de acceso con el comando `access-list 1 permit 192.168.1.0 0.0.0.255`, la última parte del comando 
(0.0.0.255) es una _wildcard_, ¿qué es una wildcard y en qué se diferencia con una máscara de red?
Es una máscara de subred, con una regla de correspondencia:
El 0 significa que se debe comprobar el bit equivalente
El 1 significa que el bit equivalente no importa.
Las máscaras wildcard son utilizadas en situaciones donde las máscaras de subred no pueden aplicar.

+ ¿Qué diferencia hay entre una dirección IP pública y una privada?

Una IP pública sirve para hacer la identificación de un dispositivos a nivel de diferentes redes. Mientras que una IP privada 
es una dirección que sirve para identificar a un dispositivo dentro de una red.

+ ¿Qué diferencia hay entre NAT estática, NAT dinámica y PAT? ¿Cuál es la que se encuentra usualmente en los routers de los 
hogares?

NAT Estático
Consiste básicamente en un tipo de NAT en el cuál se mapea una dirección IP privada con una dirección IP pública de forma 
estática. De esta manera, cada equipo en la red privada debe tener su correspondiente IP pública asignada para poder acceder 
a Internet. La principal desventaja de este esquema es que por cada equipo que se desee tenga acceso a Internet se debe 
contratar una IP pública.

NAT Dinámico 
Este tipo de NAT pretende mejorar varios aspectos del NAT estático dado que utiliza un pool de IPs públicas para 
un pool de IPs privadas que serán mapeadas de forma dinámica y a demanda. La ventaja de este esquema es que si se tienen por 
ejemplo 5 IPs públicas y 10 máquinas en la red privada, las primeras 5 máquinas en conectarse tendrán acceso a Internet.

PAT (Por Address Translation)
Es una característica del estándar NAT, que traduce conexiones TCP y UDP hechas por un host y un puerto en una red externa a 
otra dirección y puerto de la red interna.
