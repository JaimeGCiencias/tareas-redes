## Práctica3
Damián Rivera González.

(El contenido de la carpeta ejemplos, son archivos para probar el ejercicio 1 - file - ) 

Luego de encontrarse dentro de la carpte Practica03, se deberán compilar los programas
mediante el comando 'make' , el cual generará tres archivo binarios:

    file, hash, strings

###File
El archivo 'file' será el ejecutable para el primer ejercicio que dado un archivo pasado como parametro se inidicará si este es de tipo GIF, ELF, PNG, ZIP, PDF, MP3 y EXE.

Para su ejecución simplemente ingresar en terminal

    ./file <archivo> ...

Ejemplo
![](imagenes/file.png)

###Strings
El archivo 'strings' será el ejecutable para imprimir los caracteres válidos (con un rango de byte entre 32 y 126), se le debe pasar un archivo con contenido binario. Se deja como archivo de prueba el archivo "archivo"

Para su ejecución sería el siguiente uso:

    ./strings <archivo>

Ejemplo
![](imagenes/strings.png)

###Hash
El archivo 'hash' será el ejecutable para imprimir el valor Md5 o Sha1 o Sha256 de los archivos que se pasan como parametros. Consta del tipo de función hash Md5 o Sh1 o Sh256.

Para su uso sería la siguiente estructura:

    ./hash <opcion> <archivo1> <archivo2> ... <archivoN>
    donde <opcion> = md5 o sha1 o sha256

Ejemplo
![](imagenes/hash.png)

### Filtros
tcpdump:

tcpdump ether src 60:a4:4c:89:3c:a4 or ether dst fc:fb:fb:01:fa:21
tcpdump ether[6:2] == 0x8853 and ether[8:1] == 0x95 or ether[9:2] == 0x8853 and ether[11:1] == 0x95
tcpdump ip6
tcpdump arp && ether dst ff:ff:ff:ff:ff:ff
tcpdump host 127


wireshark:

eth.src == 60:a4:4c:89:3c:a4 or eth.dst == fc:fb:fb:01:fa:21
eth.src[0:3] == 88:53:95 or eth.src[3:6] == 88:53:95
eth.type == IPv6
eth.type == ARP and eth.dst == ff:ff:ff:ff:ff:ff
ip.addr == 127.0.0.0


### Extra

Como procedimiento, abrí la captura en wireshark y comence a ver el tráfico que allí se mostraban, las direcciones MAC se 
visualizan de la sección "Ethernet II" allí están las dos direcciones MAC.
Para las direcciones IP se visualizan en la sección "Internet Protocol Version 4, Src: ----, Dst: ---- "
Y para la MD5 se encuentra explicita en el frame 22, time 61.452621

¿Encontraste algo sospechoso en la captura? Una conversación

¿Cuáles son las direcciones MAC de los dos equipos que estaban cominicándose?
00:0c:29:79:af:e0
00:50:56:c0:00:08

¿Cuáles son sus direcciones IP?
172.16.16.1
172.16.16.127

¿Cuál es el MD5 del archivo escondido en la captura?
por cierto, la firma MD5 del archivo es 6fe658c629bcb4327d6c475e06d899fc.
