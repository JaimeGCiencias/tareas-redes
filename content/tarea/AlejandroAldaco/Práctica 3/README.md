# Práctica 3: Manejo de archivos binarios y captura de paquetes

## Alejandro Vargas Aldaco

### Los ejercicios 1,2,3

Vienen los archivos en la carpeta src y se compilan con el comando make

```bash
$ make
```
La ejecución y los nombres de los archivos es la misma que se especificó y se pueden ver ejemplos de su uso en el ejercicio **e1. Captura Wireshark**

---

### 4 Filtros

**Defino la interfaz con la que voy a escuchar como en1**

* Mostrar paquetes que tengan por dirección MAC origen 60:a4:4c:89:3c:a4 o que tengan por dirección MAC destino fc:fb:fb:01:fa:21.
  ```bash
  $ sudo tcpdump -i en1 -Aennvv ether src 60:a4:4c:89:3c:a4 or ether dst fc:fb:fb:01:fa:21
  ```
* Mostrar paqutes cuya dirección MAC ya sea origen o destino comience con 88:53:95.
  ```bash
    $ sudo tcpdump -i en1 -Aennvv ether[0:4]=0x885395
    ```
* Mostrar paquetes cuyo ethertype sea IPv6.
  ```bash
  $ sudo tcpdump -i en1 -Aennvv ip6
  ```
* Mostrar paqutes que tengan por ethertype a ARP y cuya dirección MAC destino sea ff:ff:ff:ff:ff:ff.
  ```bash
  $ sudo tcpdump -i en1 -Aennvv arp and ether dst ff:ff:ff:ff:ff:ff
  ```
* Mostrar paquetes cuya dirección IP empieza con 172.
  ```bash
  $ sudo tcpdump -i en1 -Aennvv host 172
  ```

---

### e1. Captura Wireshark

**1- Primero leo el archivo con strings (parte de la práctica) para \"encontrar algo sospechoso.\"**

```bash
$ ./strings captura.pcap | grep 'contra*\|md5\|archivo'
Kya esta disponible en el servidor web el archivo con la contrasena y usuario del sistema, aprecio tu pago en efectivo
Hbien, necesito una contrasena para abrir el archivo?
si, la contrasena del archivo es hola123
9por cierto, la firma MD5 del archivo es 6fe658c629bcb4327d6c475e06d899fc
```

**2- Observo que hay algo sospechoso y verifico si se envió algún Archivo**

```bash
$ ./strings captura.pcap | grep 'HTTP' -A 2
GET /secreto HTTP/1.1
Host: 172.16.16.1
Authorization: Basic cHJ1ZWJhOmhvbGExMjM=
--
JHTTP/1.1 200 OK
Date: Thu, 28 Feb 2019 21:52:10 GMT
Server: Apache
```
> *Ajá! un empleado transfirió un archivo que se llama \"secreto\"*

**3- Procedo a abrir el archivo captura.pcap en wireshark y desde el menú \"File > Export Objects > Http\" exporto el archivo secreto en la carpeta de esta práctica**

![exportar archivo wireshark](https://gitlab.com/alex.aldaco/tareas-redes/raw/master/content/tarea/AlejandroAldaco/Pr%C3%A1ctica%203/images/wireshark.png)

**4- Obtengo el hash para verificar que se trata del archivo correcto**

```bash
$ ./hash md5 secreto
6fe658c629bcb4327d6c475e06d899fc  --> "secreto"
```
**5- Averiguo que tipo de archivo es**
```bash
$ ./magia secreto
secreto: Archivo ZIP
```
**6- Lo renombro**
```bash
$ mv secreto secreto.zip
```

**7- Lo descomprimo**
```bash
$ unzip secreto.zip
Archive:  secreto.zip
[secreto.zip] secreto password:
  inflating: secreto
```
**8- Averiguo los datos del archivo descomprimido**
```bash
$ ./strings secreto
user=david&password=blackstar
```

**9- Obtenemos las ip de los que participaron en la conversación, vemos un pedazo de un mensaje y a quien se lo mandó**

```bash
$ tcpdump -Aenn  -r captura.pcap | grep 'aprecio tu pago en efectivo' -B 2
reading from file captura.pcap, link-type EN10MB (Ethernet)
15:51:26.251596 00:50:56:c0:00:08 > 00:0c:29:79:af:e0, ethertype IPv4 (0x0800), length 184: 172.16.16.1.36270 > 172.16.16.217.31337: Flags [P.], seq 7:125, ack 8, win 229, options [nop,nop,TS val 9276488 ecr 5227851], length 118
E.....@.@..y..........zi...xA.F............
...H.O.Kya esta disponible en el servidor web el archivo con la contrasena y usuario del sistema, aprecio tu pago en efectivo
```

Podemos observar que el que vendió los datos tiene la ip
`172.16.16.1` con la mac address `00:50:56:c0:00:08` y el que compró los datos tiene la ip
`172.16.16.217` con la mac address `00:0c:29:79:af:e0`

---

### Preguntas y respuestas

<dl>
  <dt>¿Encontraste algo sospechoso en la captura de tráfico? ¿Si así fue, cuál es la contraseña y usuario que fueron dadas a conocer?</dt>
  <dd>R: <em>Sí, user=david&password=blackstar</em></dd>

  <dt>¿Cuáles son las direcciones MAC de los dos equipos que estaban comunicándose?</dt>
  <dd>R: <em>Ver el punto 9: 00:50:56:c0:00:08 y 00:0c:29:79:af:e0</em></dd>

  <dt>¿Cuáles son sus direcciones IP?</dt>
  <dd>R: <em>Ver el punto 9: 172.16.16.1 y 172.16.16.217</em></dd>

  <dt>¿Cuál es el hash MD5 del archivo escondido en la captura?</dt>
  <dd>R: <em>Ver el punto 4: 6fe658c629bcb4327d6c475e06d899fc</em></dd>
</dl>
