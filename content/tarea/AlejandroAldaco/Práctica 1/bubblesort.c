#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] ) {
  int elements[argc];
  for (int i = 1; i < argc; i++ )
  {
    int c = atoi(argv[i]);
    elements[i] = c;
  }
  for (int i = 1; i <= argc; i++ )
  {
    for (int j = 1; j <= argc; j++ )
    {
      if (elements[j] > elements[j+1])
      {
        int aux = elements[j];
        elements[j] = elements[j+1];
        elements[j+1] = aux;
      }
    }
  }
  for (int i = 2; i <= argc; i++ )
  {
    printf("%i\n",elements[i] );
  }

  return 0;
}
