#include <stdio.h>
#include <string.h>

int main() {
    char linea[1024];
    FILE *fich;
    fich = fopen("passwd", "r");
    while(fgets(linea, 1024, (FILE*) fich)) {
        const char s[2] = ":";
        char *token;
        token = strtok(linea,s);
        int i = 0;
        while( token != NULL ) {
          switch (i) {
            case 0: printf("Nombre:     "); break;
            case 1: printf("Contraseña: "); break;
            case 2: printf("UID:        "); break;
            case 3: printf("GID:        "); break;
            case 4: printf("Geckos:     "); break;
            case 5: printf("Home:       "); break;
            case 6: printf("Shell:      "); break;
          }
          printf( " %s\n", token );
          token = strtok(NULL, s);
          i++;
        }
    }
    fclose(fich);
    return 0;
}
