#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 100

int isValidChar(char c){
	return 32<=c&&c<=126;
}

void append(char* s, char c){
    int len = strlen(s);
    s[len] = c;
    s[len+1] = '\0';
}

void flush(char* s){
	bzero(s,sizeof(s));
	s[0]='\0';
}

int main(int argc, char *argv[]){
	FILE* file;
	char buffer[BUFFER_SIZE+2];
	flush(buffer);
	int byte;
	if (file = fopen(argv[1], "r")){
		while ((byte = getc(file)) != EOF) {
			if(isValidChar(byte))
				append(buffer,byte);
			else{
				if(strlen(buffer)>1)
					printf("%s\n",buffer);
				flush(buffer);
			}
		}
		if(strlen(buffer)>1)
			printf("%s\n",buffer);
	}

}