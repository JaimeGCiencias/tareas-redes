#include "lista.h"
#include <stdlib.h>
#include <stdio.h>

void agrega_elemento(struct lista *lista, void *elemento){

	//Se crea un apuntador a la cabeza de la lista
	struct nodo * tmp = lista->cabeza;	

	// Si la lista es vacia se asigna el primer elemento
	// creando un nuevo nodo
	if(lista->longitud == 0){
		struct nodo * nuevo = malloc(sizeof(struct nodo));
		lista->cabeza = nuevo;
		nuevo->elemento = elemento;
		nuevo->siguiente = NULL;
	}else{
		//Se busca el ultimo elemento
		while(tmp->siguiente != NULL){
			tmp = tmp->siguiente;	
		}

		//Se agrega el "elemento" al final de la lista
		tmp->siguiente = malloc(sizeof(struct nodo));
		tmp->siguiente->elemento = elemento;
		tmp->siguiente->siguiente = NULL;
	}

	lista->longitud++;	
}

void * obten_elemento(struct lista *lista, int n){

	// Como se cuenta desde 0
	// el ultimo indice es igual a la lingitud menos 1
	if(n >= lista->longitud){
		return 0;
	}

	//Se crea un apuntador a la cabeza de la lista
	struct nodo * tmp = lista->cabeza;

	for (int i = 0; i < n; ++i){
		tmp = tmp->siguiente;		
	}

	return  tmp->elemento;
}


void * elimina_elemento(struct lista *lista, int n){

	if(lista->longitud == 0){
		return 0;
	}
	
	struct nodo * tmp = lista->cabeza;
	struct nodo * tmp_prev = lista->cabeza;

	// Caso en el que solo hay un elemento en la lista
	if(lista->longitud == 1){
		lista->cabeza = NULL;
		lista->longitud --;				
		return tmp->elemento;
	}

	// Caso en el que el elemento a eliminar se encuentra al principio
	if (n == 0){
		lista->cabeza = tmp->siguiente;
		lista->longitud--;		
		return tmp->elemento;
	}else{
		for (int i = 0; i < n; ++i){
			tmp_prev = tmp;
			tmp = tmp->siguiente;		
		}
		// Caso en el que el elemento a eliminar se encuentra al final
		if(lista->longitud-1 == n){
			tmp_prev->siguiente = NULL;
			lista->longitud--;					
		}
		// Caso en el que el elemento a eliminar se encuentra en medio
		else{
			tmp_prev->siguiente = tmp->siguiente;			
			lista->longitud--;			
		}		
		return tmp->elemento;
	}
	free(tmp);
	free(tmp_prev);

}

void aplica_funcion(struct lista *lista, void (*f)(void *)){

	struct nodo * tmp = lista->cabeza;	

	//Se recorre la lista y se aplica la funcion a cada elemento
	while(tmp != NULL){
			f(tmp->elemento);
			tmp = tmp->siguiente;	
		}
}