struct nodo{
    void * elemento;
    struct nodo * siguiente;
};

struct  lista
{
    struct nodo * cabeza;
    int longitud;
};
//Agrega un elemento un elemento al final de la lista.
void * agrega_elemento(struct lista * lista, void * elemento){
    struct nodo * n;
    n = malloc(sizeof(n));
    n -> elemento = elemento;
    struct nodo * auxiliar;
    auxiliar =  lista -> cabeza;
    if(lista->longitud != 0 && lista ->cabeza != 0){        
        while(auxiliar->siguiente != 0){
            auxiliar = auxiliar -> siguiente;
        
            }
        auxiliar ->siguiente = n;
        lista -> longitud++;
    }
    else{
        lista -> cabeza = n;
        lista -> longitud++;
    }

}
//Obtiene un elemento dado una posicion n en la lista, recive un apuntador de la lista y un entero
void * obten_elemento(struct lista * lista, int n){
   int i = 0;
   int * a = &i;
    struct nodo * auxiliar;
    auxiliar = lista ->cabeza;
    if(n == 0 && lista->longitud != 0){
        return auxiliar -> elemento;
    }
    if(auxiliar -> siguiente != 0){
        while(i != n){
            auxiliar = auxiliar -> siguiente;
            i++;
        }
        return auxiliar -> elemento;
    }else{
        return a;
    } 
}

//Elimina un elemento en la posicion n de la lista recive un apuntador a la lista y un entero
void * elimina_elemento(struct lista * lista, int n){
    
    int i = 0;
    struct nodo * auxiliar;
    struct nodo * e;
    auxiliar = lista -> cabeza;
    if(lista->longitud != 0 && lista->longitud >= n){
        if (n == 0) {
            e = auxiliar->elemento;
            lista -> cabeza = auxiliar -> siguiente;
            lista -> longitud--;
        } else {
            if (n == (lista -> longitud -1)){
                while(auxiliar->siguiente->siguiente != 0){
                    auxiliar = auxiliar -> siguiente;
                    i++;
                }
                e = auxiliar -> siguiente->elemento;
                lista -> longitud--;
            } else {
                while(i < (n - 1)){
                    auxiliar = auxiliar -> siguiente;
                    i++;
                }
                e = auxiliar -> siguiente->elemento;
                auxiliar -> siguiente = auxiliar -> siguiente -> siguiente;
                lista -> longitud--;
            }
        }
    }

    return e;
}
//Aplica una funcion a cada elemento de la lista
void aplica_funcion(struct lista * lista, void (*f)(void *)){
   
    struct nodo * auxiliar;
    auxiliar = lista->cabeza;
    while(auxiliar->siguiente != 0){
        
        f(auxiliar->elemento);
        auxiliar = auxiliar ->siguiente;
    }
    f(auxiliar->elemento);        
}